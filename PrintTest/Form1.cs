﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PrintMonitor;

namespace PrintTest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'printerDataSet.logs' table. You can move, or remove it, as needed.
            this.logsTableAdapter.Fill(this.printerDataSet.logs);

            foreach (var p in new PrinterInformationCollection())
            {
                this.CheckedListBox_Printers.Items.Add(p.PrinterName);
            }
        }

        private void CheckedListBox_Printers_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (e.NewValue == CheckState.Checked)
            {
                this.printerMonitorComponent1.AddPrinter(CheckedListBox_Printers.Items[e.Index].ToString());
            }
            else
            {
                this.printerMonitorComponent1.RemovePrinter(CheckedListBox_Printers.Items[e.Index].ToString());
            }
        }

        private void printerMonitorComponent1_JobAdded(object sender, PrintJobEventArgs e)
        {
            //if (e.PrintJob.Printing)
            //{
            //    System.Diagnostics.Trace.TraceInformation("Job added " + e.PrintJob.JobId + " called " + e.PrintJob.Document + " on " + e.PrintJob.PrinterName);

            //    this.Invoke(new MethodInvoker(delegate
            //    {
            //        DataRow row = this.printerDataSet.Tables[0].NewRow();
            //        row["document"] = e.PrintJob.Document;
            //        row["printer"] = e.PrintJob.PrinterName;
            //        row["user"] = e.PrintJob.UserName;
            //        this.printerDataSet.Tables[0].Rows.Add(row);
            //        this.tableAdapterManager.UpdateAll(this.printerDataSet);
            //    }));
            //}
        }

        private void printerMonitorComponent1_PrinterInformationChanged(object sender, PrinterEventArgs e)
        {
            //System.Diagnostics.Trace.TraceInformation("Printer information changed " + e.PrinterInformation.PrinterName + " - " + e.PrinterChangeFlags.ToString());

            //if (e.PrinterInformation.IsPrinting)
            //{
            //    this.Invoke(new MethodInvoker(delegate
            //    {
            //        DataRow row = this.printerDataSet.Tables[0].NewRow();
            //        row["document"] = "MMM";
            //        //row["document"] = e.PrinterInformation.PrintJobs[0].Document;
            //        //row["printer"] = e.PrinterInformation.PrintJobs[0].PrinterName;
            //        //row["user"] = e.PrinterInformation.PrintJobs[0].UserName;
            //        this.printerDataSet.Tables[0].Rows.Add(row);
            //        this.tableAdapterManager.UpdateAll(this.printerDataSet);
            //    }));
            //}
        }

        private void printerMonitorComponent1_JobDeleted(object sender, PrintJobEventArgs e)
        {
            System.Diagnostics.Trace.TraceInformation("Job deleted " + e.PrintJob.JobId + " called " + e.PrintJob.Document + " on " + e.PrintJob.PrinterName);
        }

        private void logsBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.logsBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.printerDataSet);

        }

        private void printerMonitorComponent1_JobSet(object sender, PrintJobEventArgs e)
        {
            if (e.PrintJob.Printed && e.PrintJob.Deleted)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    DataRow row = this.printerDataSet.Tables[0].NewRow();
                    row["document"] = e.PrintJob.Document;
                    row["printer"] = e.PrintJob.PrinterName;
                    row["user"] = e.PrintJob.UserName;
                    row["created_at"] = e.EventTime;
                    this.printerDataSet.Tables[0].Rows.Add(row);
                    this.tableAdapterManager.UpdateAll(this.printerDataSet);
                }));
            }
        }

        private void printerMonitorComponent1_JobWritten(object sender, PrintJobEventArgs e)
        {

        }
    }
}
