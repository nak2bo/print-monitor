﻿namespace PrintTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CheckedListBox_Printers = new System.Windows.Forms.CheckedListBox();
            this.printerDataSet = new PrintTest.printerDataSet();
            this.logsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.logsTableAdapter = new PrintTest.printerDataSetTableAdapters.logsTableAdapter();
            this.tableAdapterManager = new PrintTest.printerDataSetTableAdapters.TableAdapterManager();
            this.logsDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.printerMonitorComponent1 = new PrintMonitor.PrinterMonitorComponent(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.printerDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.logsDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // CheckedListBox_Printers
            // 
            this.CheckedListBox_Printers.FormattingEnabled = true;
            this.CheckedListBox_Printers.Location = new System.Drawing.Point(0, 0);
            this.CheckedListBox_Printers.Name = "CheckedListBox_Printers";
            this.CheckedListBox_Printers.Size = new System.Drawing.Size(274, 361);
            this.CheckedListBox_Printers.TabIndex = 0;
            this.CheckedListBox_Printers.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.CheckedListBox_Printers_ItemCheck);
            // 
            // printerDataSet
            // 
            this.printerDataSet.DataSetName = "printerDataSet";
            this.printerDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // logsBindingSource
            // 
            this.logsBindingSource.DataMember = "logs";
            this.logsBindingSource.DataSource = this.printerDataSet;
            // 
            // logsTableAdapter
            // 
            this.logsTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.logsTableAdapter = this.logsTableAdapter;
            this.tableAdapterManager.UpdateOrder = PrintTest.printerDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // logsDataGridView
            // 
            this.logsDataGridView.AutoGenerateColumns = false;
            this.logsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.logsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5});
            this.logsDataGridView.DataSource = this.logsBindingSource;
            this.logsDataGridView.Location = new System.Drawing.Point(280, 0);
            this.logsDataGridView.Name = "logsDataGridView";
            this.logsDataGridView.RowTemplate.Height = 24;
            this.logsDataGridView.Size = new System.Drawing.Size(1000, 311);
            this.logsDataGridView.TabIndex = 2;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "id";
            this.dataGridViewTextBoxColumn1.HeaderText = "id";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "document";
            this.dataGridViewTextBoxColumn2.HeaderText = "document";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "printer";
            this.dataGridViewTextBoxColumn3.HeaderText = "printer";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "user";
            this.dataGridViewTextBoxColumn4.HeaderText = "user";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "created_at";
            this.dataGridViewTextBoxColumn5.HeaderText = "created_at";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // printerMonitorComponent1
            // 
            this.printerMonitorComponent1.JobAdded += new PrintMonitor.PrinterMonitorComponent.PrintJobEventHandler(this.printerMonitorComponent1_JobAdded);
            this.printerMonitorComponent1.JobDeleted += new PrintMonitor.PrinterMonitorComponent.PrintJobEventHandler(this.printerMonitorComponent1_JobDeleted);
            this.printerMonitorComponent1.JobWritten += new PrintMonitor.PrinterMonitorComponent.PrintJobEventHandler(this.printerMonitorComponent1_JobWritten);
            this.printerMonitorComponent1.JobSet += new PrintMonitor.PrinterMonitorComponent.PrintJobEventHandler(this.printerMonitorComponent1_JobSet);
            this.printerMonitorComponent1.PrinterInformationChanged += new PrintMonitor.PrinterMonitorComponent.PrinterEventHandler(this.printerMonitorComponent1_PrinterInformationChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1292, 804);
            this.Controls.Add(this.logsDataGridView);
            this.Controls.Add(this.CheckedListBox_Printers);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.printerDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.logsDataGridView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckedListBox CheckedListBox_Printers;
        private PrintMonitor.PrinterMonitorComponent printerMonitorComponent1;
        private printerDataSet printerDataSet;
        private System.Windows.Forms.BindingSource logsBindingSource;
        private printerDataSetTableAdapters.logsTableAdapter logsTableAdapter;
        private printerDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.DataGridView logsDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
    }
}

