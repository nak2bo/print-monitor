﻿using System;

namespace PrintMonitor
{
    // -- SERVER_EVENTS ----------------------------------------------
    // Not implemented in this release
#if SERVER_EVENTS //= 1
    /// <summary>
    /// Generic event argument class from which the print server event arguments all derive
    /// </summary>
    /// <remarks></remarks>
    public class PrintServerEventArgs : EventArgs
    {

        #region "Private members"
        #endregion
        private DateTime _EventTime;

        #region "Public interface"
        public DateTime EventTime
        {
            get { return _EventTime; }
        }
        #endregion

        #region "Public constructor"
        public PrintServerEventArgs()
        {
            _EventTime = DateTime.Now;
        }
        #endregion

    }

    /// <summary>
    /// Provides data for the <see cref="PrintServer.PrinterDriverAdded">PrinterDriverAdded</see>, 
    /// <see cref="PrintServer.PrinterDriverRemoved">PrinterDriverRemoved</see> and 
    /// <see cref="PrintServer.PrinterDriverSet">PrinterDriverSet</see> events of the <see cref="PrintServer">Print Server</see> control
    /// </summary>
    /// <remarks>
    /// </remarks>
    public class PrintServerDriverEventArgs : PrintServerEventArgs
    {

        #region "Public enumerated types"
        public enum DriverEventTypes
        {
            DriverAddedEvent = 0,
            DriverRemovedEvent = 1,
            DriverSettingsChangedEvent = 2
        }
        #endregion

        #region "Private members"
        #endregion
        private DriverEventTypes _ChangeType;

        #region "Public interface"
        public DriverEventTypes ChangeType
        {
            get { return _ChangeType; }
        }
        #endregion

        #region "Public constructor"
        public PrintServerDriverEventArgs(Int32 Flags) : base()
        {
            if ((Flags & (int)SpoolerApiConstantEnumerations.PrinterChangeNotificationDriverFlags.PRINTER_CHANGE_ADD_PRINTER_DRIVER) != 0)
            {
                _ChangeType = DriverEventTypes.DriverAddedEvent;
            }
            else if ((Flags & (int)SpoolerApiConstantEnumerations.PrinterChangeNotificationDriverFlags.PRINTER_CHANGE_DELETE_PRINTER_DRIVER) != 0)
            {
                _ChangeType = DriverEventTypes.DriverRemovedEvent;
            }
            else
            {
                _ChangeType = DriverEventTypes.DriverSettingsChangedEvent;
            }
        }
        #endregion

    }

    /// <summary>
    /// Provides data for the <see cref="PrintServer.FormAdded">FormAdded</see>, 
    /// <see cref="PrintServer.FormRemoved">FormRemoved</see> and 
    /// <see cref="PrintServer.FormSet">FormSet</see> events of the <see cref="PrintServer">Print Server</see> control
    /// </summary>
    /// <remarks></remarks>
    public class PrintServerFormEventArgs : PrintServerEventArgs
    {

        #region "Public enumerated types"
        public enum FormEventTypes
        {
            FormAddedEvent = 0,
            FormRemovedEvent = 1,
            FormSettingsChangedEvent = 2
        }
        #endregion

        #region "Private members"
        #endregion
        private FormEventTypes _ChangeType;

        #region "Public interface"
        public FormEventTypes ChangeType
        {
            get { return _ChangeType; }
        }
        #endregion

        #region "Public constructor"
        public PrintServerFormEventArgs(Int32 Flags) : base()
        {
            if ((Flags & (int)SpoolerApiConstantEnumerations.PrinterChangeNotificationFormFlags.PRINTER_CHANGE_ADD_FORM) != 0)
            {
                _ChangeType = FormEventTypes.FormAddedEvent;
            }
            else if ((Flags & (int)SpoolerApiConstantEnumerations.PrinterChangeNotificationFormFlags.PRINTER_CHANGE_DELETE_FORM) != 0)
            {
                _ChangeType = FormEventTypes.FormRemovedEvent;
            }
            else
            {
                _ChangeType = FormEventTypes.FormSettingsChangedEvent;
            }
        }
        #endregion
    }

    /// <summary>
    /// Provides data for the <see cref="PrintServer.PortAdded">PortAdded</see> and 
    /// <see cref="PrintServer.PortRemoved">PortRemoved</see> events of the <see cref="PrintServer">Print Server</see> control
    /// </summary>
    /// <remarks></remarks>
    public class PrintServerPortEventArgs : PrintServerEventArgs
    {

        #region "Public enumerated types"
        public enum PortEventTypes
        {
            PortAddedEvent = 0,
            PortRemovedEvent = 1,
            PortSettingsChangedEvent = 2
        }
        #endregion

        #region "Private members"
        #endregion
        private PortEventTypes _ChangeType;

        #region "Public interface"
        public PortEventTypes ChangeType
        {
            get { return _ChangeType; }
        }
        #endregion

        #region "Public constructor"
        public PrintServerPortEventArgs(Int32 Flags) : base()
        {
            if ((Flags & (int)SpoolerApiConstantEnumerations.PrinterChangeNotificationPortFlags.PRINTER_CHANGE_ADD_PORT) != 0)
            {
                _ChangeType = PortEventTypes.PortAddedEvent;
            }
            else if ((Flags & (int)SpoolerApiConstantEnumerations.PrinterChangeNotificationPortFlags.PRINTER_CHANGE_DELETE_PORT) != 0)
            {
                _ChangeType = PortEventTypes.PortRemovedEvent;
            }
            else
            {
                _ChangeType = PortEventTypes.PortSettingsChangedEvent;
            }
        }
        #endregion

    }

    /// <summary>
    /// Provides data for the <see cref="PrintServer.PrintProcessorAdded">PrintProcessorAdded</see> and 
    /// <see cref="PrintServer.PrintProcessorRemoved">PrintProcessorRemoved</see>  events of the <see cref="PrintServer">Print Server</see> control
    /// </summary>
    /// <remarks></remarks>
    public class PrintServerProcessorEventArgs : PrintServerEventArgs
    {

        #region "Public enumerated types"
        public enum ProcessorEventTypes
        {
            ProcessorAddedEvent = 0,
            ProcessorRemovedEvent = 1
        }
        #endregion

        #region "Private members"
        #endregion
        private ProcessorEventTypes _ChangeType;

        #region "Public interface"
        public ProcessorEventTypes ChangeType
        {
            get { return _ChangeType; }
        }
        #endregion

        #region "Public constructor"
        public PrintServerProcessorEventArgs(Int32 Flags) : base()
        {
            if ((Flags & (int)SpoolerApiConstantEnumerations.PrinterChangeNotificationProcessorFlags.PRINTER_CHANGE_ADD_PRINT_PROCESSOR) != 0)
            {
                _ChangeType = ProcessorEventTypes.ProcessorAddedEvent;
            }
            else
            {
                _ChangeType = ProcessorEventTypes.ProcessorRemovedEvent;
            }
        }
        #endregion

    }


#endif
}