﻿using System;

namespace PrintMonitor
{
    /// -----------------------------------------------------------------------------
    /// Project	 : PrinterQueueWatch
    /// Class	 : PrintServer
    /// 
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// Class with properties pertaining to a print server
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <history>
    /// 	[Duncan]	21/11/2005	Created
    /// </history>
    /// -----------------------------------------------------------------------------
    [System.Security.SuppressUnmanagedCodeSecurity()]
    [System.Runtime.InteropServices.ComVisible(false)]
    public class PrintServer
    {

        #region "Private properties"
        private string _Servername;
        #endregion
        private PrinterChangeNotificationThread _NotificationThread;

        #region "Public Interface"

        // -- SERVER_EVENTS ----------------------------------------------
        // Not implemented in this release
#if SERVER_EVENTS  //= 1
        #region "Public events"

        #region "Event Delegates"
        #region "Printer events"
	//TODO: Add eventArgs derived classes to give extra details for these events
	[Serializable()]
	public delegate void PrinterAddedEventHandler(object sender, EventArgs args);

	[Serializable()]
	public delegate void PrinterSetEventHandler(object sender, EventArgs args);

	[Serializable()]
	public delegate void PrinterRemovedEventHandler(object sender, EventArgs args);
        #endregion

        #region "Port events"
	//TODO: Add eventArgs derived classes to give extra details for these events
	[Serializable()]
	public delegate void PortAddedEventHandler(object sender, EventArgs args);

	[Serializable()]
	public delegate void PortConfiguredEventHandler(object sender, EventArgs args);

	[Serializable()]
	public delegate void PortRemovedEventHandler(object sender, EventArgs args);
        #endregion

        #region "Print Processor events"
	//TODO: Add eventArgs derived classes to give extra details for these events
	[Serializable()]
	public delegate void PrintProcessorAddedEventHandler(object sender, EventArgs args);

	[Serializable()]
	public delegate void PrintProcessorRemovedEventHandler(object sender, EventArgs args);
        #endregion

        #region "Printer driver events"
	//TODO: Add eventArgs derived classes to give extra details for these events
	[Serializable()]
	public delegate void PrinterDriverAddedEventHandler(object sender, EventArgs args);

	[Serializable()]
	public delegate void PrinterDriverSetEventHandler(object sender, EventArgs args);

	[Serializable()]
	public delegate void PrinterDriverRemovedEventHandler(object sender, EventArgs args);
        #endregion

        #region "Form events"
	//TODO: Add eventArgs derived classes to give extra details for these events
	[Serializable()]
	public delegate void FormAddedEventHandler(object sender, EventArgs args);

	[Serializable()]
	public delegate void FormSetEventHandler(object sender, EventArgs args);

	[Serializable()]
	public delegate void FormRemovedEventHandler(object sender, EventArgs args);
        #endregion
        #endregion

        #region "Printer Events"
        #region "PrinterAdded"
	/// <summary>
	/// A <see cref="PrinterInformation">printer</see> has been added to the server being monitored
	/// </summary>
	/// <remarks></remarks>
	public event PrinterAddedEventHandler PrinterAdded;
        #endregion

        #region "PrinterSet"
	/// <summary>
	/// The device settings for a <see cref="PrinterInformation">printer</see> on the server being monitored have changed
	/// </summary>
	/// <remarks></remarks>
	public event PrinterSetEventHandler PrinterSet;
        #endregion

        #region "PrinterRemoved"
	/// <summary>
	/// A <see cref="PrinterInformation">printer</see> has been removed from the print server being monitored
	/// </summary>
	/// <remarks></remarks>
	public event PrinterRemovedEventHandler PrinterRemoved;
        #endregion
        #endregion

        #region "Port events"
        #region "PortAdded"
	/// <summary>
	/// A new <see cref="Port">printer port</see> was added to the server
	/// </summary>
	/// <remarks></remarks>
	public event PortAddedEventHandler PortAdded;
        #endregion

        #region "PortConfigured"
	/// <summary>
	/// A <see cref="Port">port</see> was configured on the server
	/// </summary>
	/// <remarks></remarks>
	public event PortConfiguredEventHandler PortConfigured;
        #endregion

        #region "PortRemoved"
	/// <summary>
	/// A <see cref="Port">port</see> was removed from the server being monitored
	/// </summary>
	/// <remarks></remarks>
	public event PortRemovedEventHandler PortRemoved;
        #endregion
        #endregion

        #region "PrintProcessor Events"
        #region "PrintProcessorAdded"
	/// <summary>
	/// A new <see cref="PrintProcessor">print processor</see> was added to the server being monitored
	/// </summary>
	/// <remarks></remarks>
	public event PrintProcessorAddedEventHandler PrintProcessorAdded;
        #endregion

        #region "PrintProcessorRemoved"
	/// <summary>
	/// A <see cref="PrintProcessor">print processor</see> was removed from this server
	/// </summary>
	/// <remarks></remarks>
	public event PrintProcessorRemovedEventHandler PrintProcessorRemoved;
        #endregion

        #endregion

        #region "PrinterDriver events"

        #region "PrinterDriverAdded"
	/// <summary>
	/// A <see cref="PrinterDriver">printer driver</see> was added to the server being monitored
	/// </summary>
	/// <remarks></remarks>
	public event PrinterDriverAddedEventHandler PrinterDriverAdded;
        #endregion

        #region "PrinterDriverSet"
	/// <summary>
	/// A <see cref="PrinterDriver">printer driver's</see> settings were changed on the server
	/// </summary>
	/// <remarks></remarks>
	public event PrinterDriverSetEventHandler PrinterDriverSet;
        #endregion

        #region "PrinterDriverRemoved"
	/// <summary>
	/// A <see cref="PrinterDriver">printer driver</see> was removed from the server being monitored
	/// </summary>
	/// <remarks></remarks>
	public event PrinterDriverRemovedEventHandler PrinterDriverRemoved;
        #endregion

        #endregion

        #region "PrintForm events"
        #region "FormAdded"
	/// <summary>
	/// A <see cref="PrinterForm">printer form</see> was added to this server
	/// </summary>
	/// <remarks></remarks>
	public event FormAddedEventHandler FormAdded;

	/// <summary>
	/// A <see cref="PrinterForm">printer form</see>'s settings were changed on this server
	/// </summary>
	/// <remarks></remarks>
	public event FormSetEventHandler FormSet;

	/// <summary>
	/// A <see cref="PrinterForm">printer form</see> was removed from this server
	/// </summary>
	/// <remarks></remarks>
	public event FormRemovedEventHandler FormRemoved;
        #endregion
        #endregion
        #endregion
#endif

        #region "Print Monitors"
        /// <summary>
        /// The print monitors installed on this print server
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// </remarks>
        /// <example> Lists the print monitors on the current machine in a list box
        /// <code>
        /// 
        ///    Dim server As New PrintServer
        ///
        ///    Me.ListBox1.Items.Clear()
        ///    For ms As Integer = 0 To server.PrintMonitors.Count - 1
        ///        Me.ListBox1.Items.Add( server.PrintMonitors(ms).Name )
        ///    Next
        /// 
        /// </code>
        /// 
        /// </example>
        /// <history>
        /// 	[Duncan]	21/11/2005	Created
        /// </history>
        public PrintMonitors PrintMonitors
        {
            get
            {
                if (_Servername == null || string.IsNullOrEmpty(_Servername))
                {
                    return new PrintMonitors();
                }
                else
                {
                    return new PrintMonitors(_Servername);
                }
            }
        }
        #endregion

        #region "Print Processors"
        /// -----------------------------------------------------------------------------
        /// <summary>
        /// The print processors installed on this print server
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// 	[Duncan]	21/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public PrintProcessorCollection PrintProcessors
        {
            get
            {
                if (_Servername == null || string.IsNullOrEmpty(_Servername))
                {
                    return new PrintProcessorCollection();
                }
                else
                {
                    return new PrintProcessorCollection(_Servername);
                }
            }
        }
        #endregion

        #region "Printer Drivers"
        /// -----------------------------------------------------------------------------
        /// <summary>
        /// The printer drivers installed on this print server
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// 	[Duncan]	21/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public PrinterDriverCollection PrinterDrivers
        {
            get
            {
                if (_Servername == null || string.IsNullOrEmpty(_Servername))
                {
                    return new PrinterDriverCollection();
                }
                else
                {
                    return new PrinterDriverCollection(_Servername);
                }
            }
        }
        #endregion

        #region "Ports"
        /// -----------------------------------------------------------------------------
        /// <summary>
        /// The print ports installed on this print server
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// 	[Duncan]	21/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public PortCollection Ports
        {
            get
            {
                if (_Servername == null || string.IsNullOrEmpty(_Servername))
                {
                    return new PortCollection();
                }
                else
                {
                    return new PortCollection(_Servername);
                }
            }
        }
        #endregion

        #region "Printers"
        /// -----------------------------------------------------------------------------
        /// <summary>
        /// The print devices installed on this print server
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// These can be both physical printers and also software print devices
        /// </remarks>
        /// <history>
        /// 	[Duncan]	21/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public PrinterInformationCollection Printers
        {
            get
            {
                if (_Servername == null || string.IsNullOrEmpty(_Servername))
                {
                    return new PrinterInformationCollection();
                }
                else
                {
                    return new PrinterInformationCollection(_Servername);
                }
            }
        }
        #endregion

        #region "ServerName"
        /// <summary>
        /// The name of the server for which this component returns printer information
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks>
        /// Set this value to blank to return information about the current server
        /// </remarks>
        public string ServerName
        {
            get { return _Servername; }
            set { _Servername = value; }
        }
        #endregion

        #endregion

        #region "Public constructors"
        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Gets print server information for the named server
        /// </summary>
        /// <param name="Servername">The server to query information for</param>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// 	[Duncan]	21/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public PrintServer(string Servername)
        {
            if (Servername.StartsWith("\\\\"))
            {
                _Servername = Servername;
            }
            else
            {
                _Servername = "\\\\" + Servername;
            }
        }

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Gets print server information for the current machine
        /// </summary>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// 	[Duncan]	21/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------

        public PrintServer()
        {
        }
        #endregion

    }
}
