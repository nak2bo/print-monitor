﻿using System;
using System.Runtime.InteropServices;
using SpoolerStructs;
using SpoolerApiConstantEnumerations;

namespace PrintMonitor
{
    static class UnsafeNativeMethods
    {
        // -- Notes: -------------------------------------------------------------------------------
        // Always use <InAttribute()> and <OutAttribute()> to cut down on unnecessary marshalling
        // -----------------------------------------------------------------------------------------
        #region "Api Declarations"

        #region "OpenPrinter"
        [DllImport("winspool.drv", EntryPoint = "OpenPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([InAttribute()] string pPrinterName, [OutAttribute()] out IntPtr phPrinter, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] PRINTER_DEFAULTS pDefault);

        [DllImport("winspool.drv", EntryPoint = "OpenPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([InAttribute()]string pPrinterName, [OutAttribute()] out IntPtr phPrinter, [InAttribute()] int pDefault);


        [DllImport("winspool.drv", EntryPoint = "OpenPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool OpenPrinter([InAttribute()] string pPrinterName, [OutAttribute()] out IntPtr phPrinter, [InAttribute()] PrinterDefaults pDefault);


        #endregion

        #region "ClosePrinter"
        [DllImport("winspool.drv", EntryPoint = "ClosePrinter", SetLastError = true, ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ClosePrinter([InAttribute()] IntPtr hPrinter);
        #endregion

        #region "GetPrinter"
        [DllImport("winspool.drv", EntryPoint = "GetPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool GetPrinter([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 Level, [OutAttribute()] IntPtr lpPrinter, [InAttribute()] Int32 cbBuf, [OutAttribute()] out IntPtr lpbSizeNeeded);
        #endregion

        #region "EnumPrinters"
        [DllImport("winspool.drv", EntryPoint = "EnumPrinters", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumPrinters([InAttribute()] EnumPrinterFlags Flags, [InAttribute()] string Name, [InAttribute()] Int32 Level, [OutAttribute()] IntPtr lpBuf, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcbReturned);

        [DllImport("winspool.drv", EntryPoint = "EnumPrinters", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumPrinters([InAttribute()] EnumPrinterFlags Flags, [InAttribute()] Int32 Name, [InAttribute()] Int32 Level, [OutAttribute()] IntPtr lpBuf, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcbReturned);

        #endregion

        #region "GetPrinterDriver"
        [DllImport("winspool.drv", EntryPoint = "GetPrinterDriver", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool GetPrinterDriver([InAttribute()] IntPtr hPrinter, [InAttribute()] string pEnvironment, [InAttribute()] Int32 Level, [OutAttribute()] IntPtr lpDriverInfo, [InAttribute()] IntPtr cbBuf, [OutAttribute()] out IntPtr lpbSizeNeeded);
        #endregion

        #region "EnumPrinterDrivers"
        [DllImport("winspool.drv", EntryPoint = "EnumPrinterDrivers", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumPrinterDrivers([InAttribute()] string ServerName, [InAttribute()] string Environment, [InAttribute()] Int32 Level, [OutAttribute()] IntPtr lpBuf, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcbReturned);
        #endregion

        #region "SetPrinter"
        [DllImport("winspool.drv", EntryPoint = "SetPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SetPrinter([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 Level, [InAttribute()] IntPtr pPrinter, [InAttribute()] PrinterControlCommands Command);

        [DllImport("winspool.drv", EntryPoint = "SetPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SetPrinter([InAttribute()] IntPtr hPrinter, [InAttribute(), MarshalAs(UnmanagedType.U4)] PrinterInfoLevels Level, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] PRINTER_INFO_1 pPrinter, [InAttribute(), MarshalAs(UnmanagedType.U4)] PrinterControlCommands Command);

        [DllImport("winspool.drv", EntryPoint = "SetPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SetPrinter([InAttribute()] IntPtr hPrinter, [InAttribute(), MarshalAs(UnmanagedType.U4)] PrinterInfoLevels Level, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] PRINTER_INFO_2 pPrinter, [InAttribute(), MarshalAs(UnmanagedType.U4)] PrinterControlCommands Command);

        [DllImport("winspool.drv", EntryPoint = "SetPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SetPrinter([InAttribute()] IntPtr hPrinter, [InAttribute(), MarshalAs(UnmanagedType.U4)] PrinterInfoLevels Level, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] PRINTER_INFO_3 pPrinter, [InAttribute(), MarshalAs(UnmanagedType.U4)] PrinterControlCommands Command);
        #endregion

        #region "GetJob"
        [DllImport("winspool.drv", EntryPoint = "GetJob", SetLastError = true, CharSet = CharSet.Auto, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool GetJob([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 dwJobId, [InAttribute()] Int32 Level, [OutAttribute()] IntPtr lpJob, [InAttribute()] Int32 cbBuf, [OutAttribute()] out IntPtr lpbSizeNeeded);



        #endregion

        #region "FindFirstPrinterChangeNotification"


        [DllImport("winspool.drv", EntryPoint = "FindFirstPrinterChangeNotification", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern Microsoft.Win32.SafeHandles.SafeWaitHandle FindFirstPrinterChangeNotification([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 fwFlags, [InAttribute()] Int32 fwOptions, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] PrinterNotifyOptions pPrinterNotifyOptions);

        [DllImport("winspool.drv", EntryPoint = "FindFirstPrinterChangeNotification", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr UnsafeFindFirstPrinterChangeNotification([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 fwFlags, [InAttribute()] Int32 fwOptions, [InAttribute()] IntPtr pPrinterNotifyOptions);

        [DllImport("winspool.drv", EntryPoint = "FindFirstPrinterChangeNotification", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern Microsoft.Win32.SafeHandles.SafeWaitHandle FindFirstPrinterChangeNotification([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 fwFlags, [InAttribute()] Int32 fwOptions, [InAttribute()] IntPtr pPrinterNotifyOptions);
        #endregion

        #region "FindNextPrinterChangeNotification"
        [DllImport("winspool.drv", EntryPoint = "FindNextPrinterChangeNotification", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FindNextPrinterChangeNotification([InAttribute()] IntPtr hChangeObject, [OutAttribute()] out Int32 pdwChange, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] PrinterNotifyOptions pPrinterNotifyOptions, [OutAttribute()] out IntPtr lppPrinterNotifyInfo);

        [DllImport("winspool.drv", EntryPoint = "FindNextPrinterChangeNotification", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FindNextPrinterChangeNotification([InAttribute()] Microsoft.Win32.SafeHandles.SafeWaitHandle hChangeObject, [OutAttribute()] out Int32 pdwChange, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] PrinterNotifyOptions pPrinterNotifyOptions, [OutAttribute()] out IntPtr lppPrinterNotifyInfo);
        #endregion

        #region "FreePrinterNotifyInfo"
        [DllImport("winspool.drv", EntryPoint = "FreePrinterNotifyInfo", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FreePrinterNotifyInfo([InAttribute()] IntPtr lppPrinterNotifyInfo);
        #endregion

        #region "FindClosePrinterChangeNotification"
        [DllImport("winspool.drv", EntryPoint = "FindClosePrinterChangeNotification", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FindClosePrinterChangeNotification([InAttribute()] Int32 hChangeObject);

        [DllImport("winspool.drv", EntryPoint = "FindClosePrinterChangeNotification", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool FindClosePrinterChangeNotification([InAttribute()] Microsoft.Win32.SafeHandles.SafeWaitHandle hChangeObject);
        #endregion

        #region "EnumJobs"
        [DllImport("winspool.drv", EntryPoint = "EnumJobs", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumJobs([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 FirstJob, [InAttribute()] Int32 NumberOfJobs, [InAttribute(), MarshalAs(UnmanagedType.U4)] JobInfoLevels Level, [OutAttribute()] IntPtr pbOut, [InAttribute()] Int32 cbIn, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcReturned);
        #endregion

        #region "SetJob"
        [DllImport("winspool.drv", EntryPoint = "SetJob", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SetJob([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 dwJobId, [InAttribute()] Int32 Level, [InAttribute()] IntPtr lpJob, [InAttribute(), MarshalAs(UnmanagedType.U4)] PrintJobControlCommands dwCommand);

        [DllImport("winspool.drv", EntryPoint = "SetJob", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SetJob(IntPtr hPrinter, Int32 dwJobId, Int32 Level, [MarshalAs(UnmanagedType.LPStruct)] JOB_INFO_1 lpJob, PrintJobControlCommands dwCommand);

        [DllImport("winspool.drv", EntryPoint = "SetJob", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SetJob(IntPtr hPrinter, Int32 dwJobId, Int32 Level, [MarshalAs(UnmanagedType.LPStruct)] JOB_INFO_2 lpJob, PrintJobControlCommands dwCommand);
        #endregion

        #region "EnumMonitors"
        [DllImport("winspool.drv", EntryPoint = "EnumMonitors", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumMonitors([InAttribute()] string ServerName, [InAttribute()] Int32 Level, [OutAttribute()] Int32 lpBuf, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcReturned);

        [DllImport("winspool.drv", EntryPoint = "EnumMonitors", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumMonitors([InAttribute()] Int32 pServerName, [InAttribute()] Int32 Level, [OutAttribute()] Int32 lpBuf, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcReturned);
        #endregion

        #region "DocumentProperties"
        [DllImport("winspool.drv", EntryPoint = "DocumentProperties", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern Int32 DocumentProperties([InAttribute()] Int32 hwnd, [InAttribute()] IntPtr hPrinter, [InAttribute()] string pPrinterName, [OutAttribute()] out Int32 pDevModeOut, [InAttribute()] Int32 pDevModeIn, [InAttribute()] DocumentPropertiesModes Mode);
        #endregion

        #region "DeviceCapabilities"
        [DllImport("winspool.drv", EntryPoint = "DeviceCapabilities", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern Int32 DeviceCapabilities([InAttribute()] string pPrinterName, [InAttribute()] string pPortName, [InAttribute(), MarshalAs(UnmanagedType.U4)] PrintDeviceCapabilitiesIndexes CapbilityIndex, [OutAttribute()] out Int32 lpOut, [InAttribute()] Int32 pDevMode);

        #endregion

        #region "GetPrinterDriverDirectory"
        [DllImport("winspool.drv", EntryPoint = "GetPrinterDriverDirectory", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool GetPrinterDriverDirectory([InAttribute()] string ServerName, [InAttribute()] string Environment, [InAttribute()] Int32 Level, [OutAttribute()] out string DriverDirectory, [InAttribute()] Int32 BufferSize, [OutAttribute()] out Int32 BytesNeeded);

        [DllImport("winspool.drv", EntryPoint = "GetPrinterDriverDirectory", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool GetPrinterDriverDirectory([InAttribute()] Int32 ServerName, [InAttribute()] Int32 Environment, [InAttribute()] Int32 Level, [OutAttribute()] out string DriverDirectory, [InAttribute()] Int32 BufferSize, [OutAttribute()] out Int32 BytesNeeded);
        #endregion

        #region "AddPrinterDriver"
        [DllImport("winspool.drv", EntryPoint = "AddPrinterDriver", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool AddPrinterDriver([InAttribute()] string ServerName, [InAttribute()] Int32 Level, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] DRIVER_INFO_2 pDriverInfo);
        #endregion

        #region "EnumPorts"
        [DllImport("winspool.drv", EntryPoint = "EnumPorts", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumPorts([InAttribute()] string ServerName, [InAttribute()] Int32 Level, [OutAttribute()] Int32 pbOut, [InAttribute()] Int32 cbIn, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcReturned);

        [DllImport("winspool.drv", EntryPoint = "EnumPorts", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumPorts([InAttribute()] Int32 ServerName, [InAttribute()] Int32 Level, [OutAttribute()] Int32 pbOut, [InAttribute()] Int32 cbIn, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcReturned);
        #endregion

        #region "SetPort"
        [DllImport("winspool.drv", EntryPoint = "SetPort", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SetPort([InAttribute()] string ServerName, [InAttribute()] string PortName, [InAttribute()] long Level, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] PORT_INFO_3 PortInfo);
        #endregion

        #region "EnumPrintProcessors"
        [DllImport("winspool.drv", EntryPoint = "EnumPrintProcessors", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumPrintProcessors([InAttribute()] string ServerName, [InAttribute()] string Environment, [InAttribute()] Int32 Level, [OutAttribute()] Int32 lpBuf, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcbReturned);
        #endregion

        #region "EnumPrintProcessorDataTypes"
        [DllImport("winspool.drv", EntryPoint = "EnumPrintProcessorDatatypes", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumPrinterProcessorDataTypes([InAttribute()] string ServerName, [InAttribute()] string PrintProcessorName, [InAttribute()] Int32 Level, [OutAttribute()] Int32 pDataTypes, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcReturned);
        #endregion

        #region "GetForm"
        [DllImport("winspool.drv", EntryPoint = "GetForm", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool GetForm([InAttribute()] Int32 PrinterHandle, [InAttribute()] string FormName, [InAttribute()] Int32 Level, [OutAttribute()] out FORM_INFO_1 pForm, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded);
        #endregion

        #region "SetForm"
        [DllImport("winspool.drv", EntryPoint = "SetForm", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool SetForm([InAttribute()] IntPtr PrinterHandle, [InAttribute()] string FormName, [InAttribute()] Int32 Level, [InAttribute()] FORM_INFO_1 pForm);
        #endregion

        #region "EnumForms"
        [DllImport("winspool.drv", EntryPoint = "EnumForms", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EnumForms([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 Level, [OutAttribute()] IntPtr pForm, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded, [OutAttribute()] out Int32 pcFormsReturned);
        #endregion

        #region "ReadPrinter"
        [DllImport("winspool.drv", EntryPoint = "ReadPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool ReadPrinter([InAttribute()] IntPtr hPrinter, [OutAttribute()] IntPtr pBuffer, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded);

        #endregion

        #region "WritePrinter"
        [DllImport("winspool.drv", EntryPoint = "WritePrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool WritePrinter([InAttribute()] IntPtr hPrinter, [OutAttribute()] IntPtr pBuffer, [InAttribute()] Int32 cbBuf, [OutAttribute()] out Int32 pcbNeeded);
        #endregion

        #region "StartDocPrinter"
        [DllImport("winspool.drv", EntryPoint = "StartDocPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartDocPrinter([InAttribute()] IntPtr hPrinter, [InAttribute()] Int32 Level, [InAttribute(), MarshalAs(UnmanagedType.LPStruct)] DOC_INFO_1 DocInfo);
        #endregion

        #region "EndDocPrinter"
        [DllImport("winspool.drv", EntryPoint = "EndDocPrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndDocPrinter([InAttribute()] IntPtr hPrinter);
        #endregion

        #region "StartPagePrinter"
        [DllImport("winspool.drv", EntryPoint = "StartPagePrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool StartPagePrinter([InAttribute()] IntPtr hPrinter);
        #endregion

        #region "EndPagePrinter"
        [DllImport("winspool.drv", EntryPoint = "EndPagePrinter", SetLastError = true, CharSet = CharSet.Unicode, ExactSpelling = false, CallingConvention = CallingConvention.StdCall)]
        public static extern bool EndPagePrinter([InAttribute()] IntPtr hPrinter);
        #endregion

        #endregion

    }
}
