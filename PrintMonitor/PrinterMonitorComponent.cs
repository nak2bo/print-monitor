﻿using SpoolerApiConstantEnumerations;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace PrintMonitor
{
    [ToolboxBitmap(typeof(PrinterMonitorComponent), "toolboximage.bmp"), System.Security.SuppressUnmanagedCodeSecurity()]
    public partial class PrinterMonitorComponent : Component
    {
        public static TraceSwitch ComponentTraceSwitch = new TraceSwitch("PrinterMonitorComponent", "Printer Monitor Component Tracing");
        public enum MonitorJobEventInformationLevels
        {
            MaximumJobInformation = 1,
            MinimumJobInformation = 2,
            NoJobInformation = 3
        }
        private const int DEFAULT_THREAD_TIMEOUT = 1000;
        private const uint INFINITE_THREAD_TIMEOUT = 0xffffffff;
        private const int PRINTER_NOTIFY_OPTIONS_REFRESH = 0x1;
        private IntPtr mhPrinter;
        private string msDeviceName;
        private int _WatchFlags = (int)(PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_JOB | PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_PRINTER);
        private MonitorJobEventInformationLevels _MonitorJobInfoLevel = MonitorJobEventInformationLevels.MaximumJobInformation;
        private int _ThreadTimeout = DEFAULT_THREAD_TIMEOUT;
        private PrinterInformation piOut;
        private MonitoredPrinters _MonitoredPrinters;
        private bool _SpoolMonitoringDisabled = false;
        public static System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PrinterMonitorComponent));
        [Serializable()]
        public delegate void PrintJobEventHandler(object sender, PrintJobEventArgs e);
        [Serializable()]
        public delegate void PrinterEventHandler(object sender, PrinterEventArgs e);
        public event PrintJobEventHandler JobAdded;
        public event PrintJobEventHandler JobDeleted;
        public event PrintJobEventHandler JobWritten;
        public event PrintJobEventHandler JobSet;
#if SPOOL_MONITORING_ENABLED
        public event EventHandler PrintJobSpoolfileParsed;
#endif
        public event PrinterEventHandler PrinterInformationChanged;
        public delegate void JobEvent(PrintJobEventArgs e);
        public delegate void PrinterEvent(PrinterEventArgs e);

        public PrinterMonitorComponent()
        {
            InitializeComponent();

            if (ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("-- Printer Monitor Component ----------------- ");
                Trace.WriteLine(" Started : " + DateTime.Now.ToLongDateString());
                Trace.WriteLine(" Version : " + System.Windows.Forms.Application.ProductVersion);
                Trace.WriteLine(" --------------------------------------------- ");
            }

#if SPOOL_MONITORING_ENABLED
		    //\\ Start listening for any incoming spool responses
		    int _Port = 8913;
		    System.Configuration.AppSettingsReader configSettings = new System.Configuration.AppSettingsReader();
		    try {
			    var _with1 = configSettings;
			    _Port = Convert.ToInt32(_with1.GetValue("ReturnPort", typeof(System.Int32)));
			    SpoolReciever = new SpoolTCPReceiver(_Port);
		    } catch (Exception ex) {
			    if (ComponentTraceSwitch.TraceError) {
				    Trace.WriteLine(ex.ToString, this.GetType().ToString());
			    }
			    //\\ Don't try and parse spool files if no return port specified
			    SpoolMonitoringDisabled = true;
		    }
#endif
        }

        public PrinterMonitorComponent(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                Disconnect();

                if ((components != null))
                {
                    try
                    {
                        components.Dispose();
                    }
                    catch (Exception ex)
                    {
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                        {
                            Trace.WriteLine("Error in Dispose " + ex.ToString(), this.GetType().ToString());
                        }
                    }
                }

                if ((_MonitoredPrinters != null))
                {
                    _MonitoredPrinters.Dispose();
                }
            }

            base.Dispose(disposing);
        }

        protected void OnJobEvent(PrintJobEventArgs e)
        {
            if (e.EventType == PrintJobEventArgs.PrintJobEventTypes.JobAddedEvent)
            {
                if (JobAdded != null)
                {
                    JobAdded(this, e);
                }

            }
            else if (e.EventType == PrintJobEventArgs.PrintJobEventTypes.JobSetEvent)
            {
                if (JobSet != null)
                {
                    JobSet(this, e);
                }
            }
            else if (e.EventType == PrintJobEventArgs.PrintJobEventTypes.JobWrittenEvent)
            {
                if (JobWritten != null)
                {
                    JobWritten(this, e);
                }
            }
            else if (e.EventType == PrintJobEventArgs.PrintJobEventTypes.JobDeletedEvent)
            {
                if (JobDeleted != null)
                {
                    JobDeleted(this, e);
                }
            }
        }

#if SPOOL_MONITORING_ENABLED
        protected void OnSpoolfileParsed(SpoolResponseEventArgs e)
	    {
		    if (PrintJobSpoolfileParsed != null) {
			    PrintJobSpoolfileParsed(this, e);
		    }
	    }
#endif
        protected void OnPrinterInformationChanged(PrinterEventArgs e)
        {
            if (PrinterInformationChanged != null)
            {
                PrinterInformationChanged(this, e);
            }
        }

        public bool Monitoring
        {
            get
            {
                if (_MonitoredPrinters == null)
                {
                    return false;
                }
                else
                {
                    return (_MonitoredPrinters.Count > 0);
                }
            }
        }

        [Browsable(false)]
        [DesignerSerializationVisibilityAttribute(DesignerSerializationVisibility.Hidden)]
        public string DeviceName
        {
            get
            {
                if ((_MonitoredPrinters != null) || _MonitoredPrinters.Count > 0)
                {
                    return _MonitoredPrinters[0].PrinterName;
                }
                else
                {
                    return "";
                }
            }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    return;
                }
                if (_MonitoredPrinters == null)
                {


#if SPOOL_MONITORING_ENABLED
                    _MonitoredPrinters = new MonitoredPrinters(OnPrinterInformationChanged, OnJobEvent, OnSpoolfileParsed);
#else
                    _MonitoredPrinters = new MonitoredPrinters(OnPrinterInformationChanged, OnJobEvent);
#endif
                }
                if (!value.Equals(msDeviceName))
                {
                    if (!string.IsNullOrEmpty(msDeviceName))
                    {
                        _MonitoredPrinters.Remove(msDeviceName);
                    }
                    msDeviceName = value;
                    if (base.DesignMode == false)
                    {
                        if (!string.IsNullOrEmpty(msDeviceName))
                        {
                            _MonitoredPrinters.Add(msDeviceName, new PrinterInformation(msDeviceName, PrinterAccessRights.PRINTER_ALL_ACCESS | PrinterAccessRights.SERVER_ALL_ACCESS, _ThreadTimeout, _MonitorJobInfoLevel, _WatchFlags));
                        }
                    }
                }
            }
        }

        [Browsable(false)]
        public PrintJobCollection PrintJobs()
        {
            //get
            //{
            if (_MonitoredPrinters.Count > 0)
            {
                return _MonitoredPrinters[0].PrintJobs;
            }
            else
            {
                return new PrintJobCollection();
            }
            //}
        }

        [Browsable(false)]
        public PrintJobCollection PrintJobs(string DeviceName)
        {
            //get
            //{
            if (_MonitoredPrinters.Contains(DeviceName))
            {
                return _MonitoredPrinters[DeviceName].PrintJobs;
            }
            else
            {
                return new PrintJobCollection();
            }
            //}
        }

        [Browsable(false)]
        public PrinterInformation PrinterInformation()
        {
            //get
            //{
            if (_MonitoredPrinters.Count > 0)
            {
                return _MonitoredPrinters[0];
            }
            else
            {
                return null;
            }
            //}
        }

        [Browsable(false)]
        public PrinterInformation PrinterInformation(string DeviceName)
        {
            //get
            //{
            if (_MonitoredPrinters.Count > 0)
            {
                return _MonitoredPrinters[DeviceName];
            }

            throw new ArgumentException("Printer information not found for this device");
            //}
        }

        [Description("The number of jobs on the queued on the printer being monitored")]
        public Int32 JobCount
        {
            get
            {
                if (mhPrinter.ToInt64() != 0)
                {
                    return this.PrinterInformation().JobCount;
                }
                else
                {
                    return 0;
                }
            }
        }

        [Description("Adds the printer to the internal list and starts monitoring it")]
        public void AddPrinter(string DeviceName)
        {
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("AddPrinter(" + DeviceName + ")", this.GetType().ToString());
            }

            if (_MonitoredPrinters == null)
            {
#if SPOOL_MONITORING_ENABLED		
			_MonitoredPrinters = new MonitoredPrinters(OnPrinterInformationChanged, OnJobEvent, OnSpoolfileParsed);
#else
                _MonitoredPrinters = new MonitoredPrinters(OnPrinterInformationChanged, OnJobEvent);
#endif
            }

            PrinterInformation piTest = new PrinterInformation(DeviceName, PrinterAccessRights.PRINTER_ACCESS_USE | PrinterAccessRights.READ_CONTROL, false, false);
            try
            {
                if (piTest.IsNetworkPrinter)
                {
                    _MonitoredPrinters.Add(DeviceName, new PrinterInformation(DeviceName, PrinterAccessRights.PRINTER_ALL_ACCESS | PrinterAccessRights.SERVER_ALL_ACCESS | PrinterAccessRights.READ_CONTROL, _ThreadTimeout, _MonitorJobInfoLevel, _WatchFlags));
                }
                else
                {
                    _MonitoredPrinters.Add(DeviceName, new PrinterInformation(DeviceName, PrinterAccessRights.PRINTER_ALL_ACCESS | PrinterAccessRights.READ_CONTROL, _ThreadTimeout, _MonitorJobInfoLevel, _WatchFlags));
                }
            }
            catch (Win32Exception ea)
            {
                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                {
                    Trace.WriteLine("AddPrinter(" + DeviceName + ") failed. " + ea.ToString(), this.GetType().ToString());
                }
            }
            piTest.Dispose();
        }

        [Description("Removes a printer from the internal list, stopping monitoring as appropriate ")]
        public void RemovePrinter(string DeviceName)
        {
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("RemovePrinter(" + DeviceName + ")", this.GetType().ToString());
            }
            _MonitoredPrinters.Remove(DeviceName);
        }

        [Description("Disconnects from all printers being monitored")]
        public void Disconnect()
        {
            if ((_MonitoredPrinters != null))
            {
                int nCount = _MonitoredPrinters.Count;
                for (int n = nCount; n >= 1; n += -1)
                {
                    _MonitoredPrinters[n - 1].Monitored = false;
                }
                _MonitoredPrinters.Clear();
                _MonitoredPrinters = null;
            }
        }

#if SPOOL_MONITORING_ENABLED
        public void RequestSpoolParse(string Printername, int JobId, bool GetPageCount)
	    {
		    if (ComponentTraceSwitch.TraceVerbose) {
			    Trace.WriteLine("RequestSpoolParse", this.GetType().ToString());
		    }
		    if (_SpoolMonitoringDisabled) {
			    if (ComponentTraceSwitch.TraceWarning) {
				    Trace.WriteLine("RequestSpoolParse : SpoolMonitoringDisabled = True", this.GetType().ToString());
			    }
			    return;
		    }
		    if ((_MonitoredPrinters != null)) {
			    if (_MonitoredPrinters.Count > 0) {
				    try {
					    _MonitoredPrinters.RequestSpoolParse(Printername, JobId, GetPageCount);
				    } catch (Exception ex) {
					    if (ComponentTraceSwitch.TraceError) {
						    Trace.WriteLine(ex.ToString, this.GetType().ToString());
					    }
					    //\\ Once an error has occured talking to the spool monitor, don't try again
					    SpoolMonitoringDisabled = true;
				    }
			    }
		    }
	    }
#endif

#if SPOOL_MONITORING_ENABLED
	    public bool SpoolMonitoringDisabled
        {
		    set { _SpoolMonitoringDisabled = value; }
	    }
#endif

        [Category("Performance Tuning"), Description("Set to make the component monitor jobs being added to the job queue"), DefaultValue(typeof(bool), "True")]
        public bool MonitorJobAddedEvent
        {
            get { return ((_WatchFlags & (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_ADD_JOB) != 0); }
            set
            {
                if (Monitoring)
                {
                    throw new ReadOnlyException("This property cannot be set once the component is monitoring a print queue");
                }
                else
                {
                    if (value)
                    {
                        _WatchFlags = _WatchFlags | (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_ADD_JOB;
                    }
                    else
                    {
                        _WatchFlags = _WatchFlags & (int) ~PrinterChangeNotificationJobFlags.PRINTER_CHANGE_ADD_JOB;
                    }
                }
            }
        }

        [Category("Performance Tuning"), Description("Set to make the component monitor jobs being removed from the job queue"), DefaultValue(typeof(bool), "True")]
        public bool MonitorJobDeletedEvent
        {
            get { return ((_WatchFlags & (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_DELETE_JOB) != 0); }
            set
            {
                if (Monitoring)
                {
                    throw new ReadOnlyException("This property cannot be set once the component is monitoring a print queue");
                }
                else
                {
                    if (value)
                    {
                        _WatchFlags = _WatchFlags | (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_DELETE_JOB;
                    }
                    else
                    {
                        _WatchFlags = _WatchFlags & (int) ~PrinterChangeNotificationJobFlags.PRINTER_CHANGE_DELETE_JOB;
                    }
                }
            }
        }

        [Category("Performance Tuning"), Description("Set to make the component monitor jobs being written on the job queue"), DefaultValue(typeof(bool), "True")]
        public bool MonitorJobWrittenEvent
        {
            get { return ((_WatchFlags & (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_WRITE_JOB) != 0); }
            set
            {
                if (Monitoring)
                {
                    throw new ReadOnlyException("This property cannot be set once the component is monitoring a print queue");
                }
                else
                {
                    if (value)
                    {
                        _WatchFlags = _WatchFlags | (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_WRITE_JOB;
                    }
                    else
                    {
                        _WatchFlags = _WatchFlags & (int) ~PrinterChangeNotificationJobFlags.PRINTER_CHANGE_WRITE_JOB;
                    }
                }
            }
        }

        [Category("Performance Tuning"), Description("Set to make the component monitor changes to the jobs on the job queue"), DefaultValue(typeof(bool), "True")]
        public bool MonitorJobSetEvent
        {
            get { return ((_WatchFlags & (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_SET_JOB) != 0); }
            set
            {
                if (Monitoring)
                {
                    throw new ReadOnlyException("This property cannot be set once the component is monitoring a print queue");
                }
                else
                {
                    if (value)
                    {
                        _WatchFlags = _WatchFlags | (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_SET_JOB;
                    }
                    else
                    {
                        _WatchFlags = _WatchFlags & (int) ~PrinterChangeNotificationJobFlags.PRINTER_CHANGE_SET_JOB;
                    }
                }
            }
        }

        [Category("Performance Tuning"), Description("Set to make the component monitor printer setup change events"), DefaultValue(typeof(bool), "True")]
        public bool MonitorPrinterChangeEvent
        {
            get { return ((_WatchFlags & (int)PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_PRINTER) != 0); }
            set
            {
                if (Monitoring)
                {
                    throw new ReadOnlyException("This property cannot be set once the component is monitoring a print queue");
                }
                else
                {
                    if (value)
                    {
                        _WatchFlags = _WatchFlags | (int)PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_PRINTER;
                    }
                    else
                    {
                        _WatchFlags = _WatchFlags & (int) ~PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_PRINTER;
                    }
                }
            }
        }

        [Category("Performance Tuning"), Description("Set to fine tune the job information required for networks"), DefaultValue(MonitorJobEventInformationLevels.MaximumJobInformation)]
        public MonitorJobEventInformationLevels MonitorJobEventInformationLevel
        {
            get { return _MonitorJobInfoLevel; }
            set
            {
                if (Monitoring)
                {
                    throw new ReadOnlyException("This property cannot be set once the component is monitoring a print queue");
                }
                else
                {
                    _MonitorJobInfoLevel = value;
                }
            }
        }

        [Category("Performance Tuning"), Description("Set to tune the printer watch refresh interval"), DefaultValue(DEFAULT_THREAD_TIMEOUT), Obsolete("This property no longer affects the operation of the component")]
        public int ThreadTimeout
        {
            get
            {
                if (_ThreadTimeout == 0 | _ThreadTimeout < -1)
                {
                    unchecked
                    {
                        _ThreadTimeout = unchecked((int)INFINITE_THREAD_TIMEOUT);
                    }
                }
                return _ThreadTimeout;
            }
            set
            {
                if (ComponentTraceSwitch.TraceWarning)
                {
                    Trace.WriteLine("Obsolete property ThreadTimeout set", this.GetType().ToString());
                }
                _ThreadTimeout = unchecked((int)INFINITE_THREAD_TIMEOUT);
            }
        }

#if SPOOL_MONITORING_ENABLED
	private void SpoolReciever_SpoolResponse(object sender, System.EventArgs e)
	{
		if (ComponentTraceSwitch.TraceVerbose || SpoolSocket.SpoolSocketTraceSwitch.TraceVerbose) {
			Trace.WriteLine("Spoolresponse recieved", this.GetType().ToString());
		}
		OnSpoolfileParsed((SpoolResponseEventArgs)e);
	}
#endif

        private System.DateTime LicenseExpiryDate()
        {
            try
            {
                Assembly foo = Assembly.GetAssembly(typeof(PrinterMonitorComponent));
                FileInfo fi = new FileInfo(foo.GetLoadedModules(false)[0].FullyQualifiedName);
                return fi.LastWriteTime.AddMonths(6);
            }
            catch (Exception ex)
            {
                return DateTime.Parse("2006-01-01");
            }
        }
    }

    #region "PrinterEventFlagDecoder class"
    internal class PrinterEventFlagDecoder
    {
        #region "Private Member Variables"


        private const int PRINTER_NOTIFY_INFO_DISCARDED = 0x1;

        private int mflags;
        #endregion

        #region "Public interface"
        /// <summary>
        /// Returns true if the printer notification message was complete
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool IsInfoComplete
        {
            get { return ((mflags & PRINTER_NOTIFY_INFO_DISCARDED) == 0); }
        }

        public bool ChangesOccured
        {
            get { return !(mflags == 0); }
        }

        internal int JobChange
        {
            get { return (mflags & (int)PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_JOB); }
        }

        internal int PrinterChange
        {
            get { return (mflags & (int)PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_PRINTER); }
        }

        internal int ProcessorChange
        {
            get { return (mflags & (int)PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_PRINT_PROCESSOR); }
        }

        internal int DriverChange
        {
            get { return (mflags & (int)PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_PRINTER_DRIVER); }
        }

        internal int FormChange
        {
            get { return (mflags & (int)PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_FORM); }
        }

        internal int PortChange
        {
            get { return (mflags & (int)PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_PORT); }
        }

        #region "Job Change events"
        public bool JobAdded
        {
            get { return ((mflags & (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_ADD_JOB) != 0); }
        }

        public bool JobDeleted
        {
            get { return ((mflags & (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_DELETE_JOB) != 0); }
        }

        public bool JobWritten
        {
            get { return ((mflags & (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_WRITE_JOB) != 0); }
        }

        public bool JobSet
        {
            get { return ((mflags & (int)PrinterChangeNotificationJobFlags.PRINTER_CHANGE_SET_JOB) != 0); }
        }
        #endregion

        #region "Printer Change events"
        /// <summary>
        /// A printer was added to the server being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool PrinterAdded
        {
            get { return ((mflags & (int)PrinterChangeNotificationPrinterFlags.PRINTER_CHANGE_ADD_PRINTER) != 0); }
        }

        /// <summary>
        /// A printer was removed from the server being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool PrinterDeleted
        {
            get { return ((mflags & (int)PrinterChangeNotificationPrinterFlags.PRINTER_CHANGE_DELETE_PRINTER) != 0); }
        }

        /// <summary>
        /// The settings of a printer on the server being monitored were changed
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool PrinterSet
        {
            get { return ((mflags & (int)PrinterChangeNotificationPrinterFlags.PRINTER_CHANGE_SET_PRINTER) != 0); }
        }

        /// <summary>
        /// A printer connection error occured on the server being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool PrinterConnectionFailed
        {
            get { return ((mflags & (int)PrinterChangeNotificationPrinterFlags.PRINTER_CHANGE_FAILED_CONNECTION_PRINTER) != 0); }
        }
        #endregion

        #region "Server change events"
        #region "Server Port Events"
        /// <summary>
        /// A new printer port was added to the server being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerPortAdded
        {
            get { return ((mflags & (int)PrinterChangeNotificationPortFlags.PRINTER_CHANGE_ADD_PORT) != 0); }
        }

        /// <summary>
        /// The settings for one of the ports on the server being monitored changed
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerPortSet
        {
            get { return ((mflags & (int)PrinterChangeNotificationPortFlags.PRINTER_CHANGE_CONFIGURE_PORT) != 0); }
        }

        /// <summary>
        /// A port was removed from the server being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerPortDeleted
        {
            get { return ((mflags & (int)PrinterChangeNotificationPortFlags.PRINTER_CHANGE_DELETE_PORT) != 0); }
        }
        #endregion

        #region "Server form events"
        /// <summary>
        /// A from was added to the server being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerFormAdded
        {
            get { return ((mflags & (int)PrinterChangeNotificationFormFlags.PRINTER_CHANGE_ADD_FORM) != 0); }
        }

        /// <summary>
        /// A form was removed from the server being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerFormDeleted
        {
            get { return ((mflags & (int)PrinterChangeNotificationFormFlags.PRINTER_CHANGE_DELETE_FORM) != 0); }
        }

        /// <summary>
        /// The properties of a form on the server being monitored were changed
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerFormSet
        {
            get { return ((mflags & (int)PrinterChangeNotificationFormFlags.PRINTER_CHANGE_SET_FORM) != 0); }
        }
        #endregion

        #region "Server processor events"
        /// <summary>
        /// A print processor was added to the server being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerProcessorAdded
        {
            get { return ((mflags & (int)PrinterChangeNotificationProcessorFlags.PRINTER_CHANGE_ADD_PRINT_PROCESSOR) != 0); }
        }

        /// <summary>
        /// A print processor was removed from the printer being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerProcessorDeleted
        {
            get { return ((mflags & (int)PrinterChangeNotificationProcessorFlags.PRINTER_CHANGE_DELETE_PRINT_PROCESSOR) != 0); }
        }
        #endregion

        #region "Server driver events"
        /// <summary>
        /// A new printer driver was added to the printer being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerDriverAdded
        {
            get { return ((mflags & (int)PrinterChangeNotificationDriverFlags.PRINTER_CHANGE_ADD_PRINTER_DRIVER) != 0); }
        }

        /// <summary>
        /// A printer driver was uninstalled from the server being monitored
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerDriverDeleted
        {
            get { return ((mflags & (int)PrinterChangeNotificationDriverFlags.PRINTER_CHANGE_DELETE_PRINTER_DRIVER) != 0); }
        }

        /// <summary>
        /// The settings for a printer driver installed on the server being monitored were changed
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool ServerDriverSet
        {
            get { return ((mflags & (int)PrinterChangeNotificationDriverFlags.PRINTER_CHANGE_SET_PRINTER_DRIVER) != 0); }
        }
        #endregion

        #endregion
        #endregion

        #region "Public constructor"
        public PrinterEventFlagDecoder(IntPtr flags)
        {
            mflags = flags.ToInt32();
        }

        public PrinterEventFlagDecoder(int flags)
        {
            mflags = flags;
        }
        #endregion

    }
    #endregion

    #region "MonitoredPrinters collection class"
    /// <summary>
    /// A type safe collection of PrinterInformation objects representing 
    /// the printers being monitored by any given PrinterMonitorComponent
    /// Unique key is Printer.DeviceName
    /// </summary>
    /// <remarks></remarks>
    internal class MonitoredPrinters : IDisposable
    {

        #region "Private member variables"
        private System.Collections.Generic.SortedList<string, PrinterInformation> _PrinterList = new System.Collections.Generic.SortedList<string, PrinterInformation>();
        private PrinterMonitorComponent.JobEvent _JobEvent;
        private PrinterMonitorComponent.PrinterEvent _PrinterEvent;
        #endregion
        private SortedList _SpoolfileParsers = new SortedList();

        #region "Public properties"
        public PrinterInformation this[string DeviceName]
        {
            get
            {
                if (DeviceName == null)
                {
                    throw new ArgumentNullException("DeviceName");
                }
                else if (string.IsNullOrEmpty(DeviceName))
                {
                    throw new ArgumentException("DeviceName cannot be blank");
                }
                else
                {
                    return (PrinterInformation)_PrinterList[DeviceName];
                }
            }
        }

        public PrinterInformation this[int Index]
        {
            get { return (PrinterInformation)_PrinterList.Values[Index]; }
        }

        public int Count
        {
            get { return _PrinterList.Count; }
        }

        #endregion

        #region "Public methods"
        #region "Add"
        public void Add(string DeviceName, PrinterInformation PrinterInformation)
        {
            if (!_PrinterList.ContainsKey(DeviceName))
            {
                _PrinterList.Add(DeviceName, PrinterInformation);
                var _with2 = this[DeviceName];
                //\\ Make the PrinterInformation class know that this component is it's event target
                _with2.InitialiseEventQueue(_JobEvent, _PrinterEvent);
                //\\ Make the printerinformation class start monitoring
                _with2.Monitored = true;
            }
        }
        #endregion
        #region "Remove"
        public void Remove(string DeviceName)
        {
            if (_PrinterList.ContainsKey(DeviceName))
            {
                ((PrinterInformation)_PrinterList[DeviceName]).Monitored = false;
                RemoveAt(_PrinterList.IndexOfKey(DeviceName));
            }
            if (_SpoolfileParsers.ContainsKey(DeviceName))
            {
                _SpoolfileParsers.RemoveAt(_SpoolfileParsers.IndexOfKey(DeviceName));
            }
        }

        public void RemoveAt(int Index)
        {
            _PrinterList.Values[Index].Monitored = false;
            _PrinterList.RemoveAt(Index);
        }
        #endregion
        #region "Contains"
        public bool Contains(string Devicename)
        {
            return _PrinterList.ContainsKey(Devicename);
        }
        #endregion
        #region "Clear"

        public void Clear()
        {
            foreach (PrinterInformation p in _PrinterList.Values)
            {
                try
                {
                    p.Dispose();
                }
                catch (Exception e)
                {
                    if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                    {
                        Trace.WriteLine("Error in Dispose of " + p.PrinterName + ":: " + e.ToString(), this.GetType().ToString());
                    }
                }
            }
            _PrinterList.Clear();
        }
        #endregion

        #region "IDisposable interface implementation"
        public void Dispose()
        {
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("Dispose()", this.GetType().ToString());
            }
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Clear();
            }
        }

        //protected override void Finalize()
        //{
        //    Dispose(false);
        //}

        ~MonitoredPrinters()
        {
            Dispose(false);
        }

        #endregion

        #endregion

        #region "Public constructor"
        public MonitoredPrinters(PrinterMonitorComponent.PrinterEvent PrinterEventCallback, PrinterMonitorComponent.JobEvent JobEventCallback)
        {
            _PrinterEvent = PrinterEventCallback;
            _JobEvent = JobEventCallback;
        }
        #endregion
    }
    #endregion
}
