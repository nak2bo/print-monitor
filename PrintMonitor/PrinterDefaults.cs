﻿using System;
using System.Runtime.InteropServices;
using SpoolerApiConstantEnumerations;

namespace PrintMonitor
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    class PrinterDefaults
    {

        #region "Public interface"

        public string DataType;
        public Int32 lpDevMode;

        public PrinterAccessRights DesiredAccess;
        #endregion

    }
}