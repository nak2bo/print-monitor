﻿using System;
using System.Diagnostics;
using SpoolerStructs;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Drawing;

namespace PrintMonitor
{
    /// -----------------------------------------------------------------------------
    /// Project	 : PrinterQueueWatch
    /// Class	 : PrinterForm
    /// 
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// Represents a form that has been installed on a printer
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <example>List the print forms on the named printer
    /// <code>
    ///        Dim pi As New PrinterInformation("Microsoft Office Document Image Writer", SpoolerApiConstantEnumerations.PrinterAccessRights.PRINTER_ALL_ACCESS, True)
    ///
    ///        For pf As Integer = 0 to pi.PrinterForms.Count -1
    ///            pi.PrinterForms(pf).Name
    ///        Next
    /// </code>
    /// </example>
    /// <history>
    /// 	[Duncan]	19/11/2005	Created
    /// </history>
    /// -----------------------------------------------------------------------------
    [System.Runtime.InteropServices.ComVisible(false)]
    public class PrinterForm
    {

        #region "Private properties"
        private IntPtr _hPrinter;
        #endregion
        private FORM_INFO_1 _fi1 = new FORM_INFO_1();

        #region "Public interface"

        #region "Name"
        /// -----------------------------------------------------------------------------
        /// <summary>
        /// The name of the printer form 
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// </remarks>
        /// <history>
        /// 	[Duncan]	19/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public string Name
        {
            get { return _fi1.Name; }
        }
        #endregion

        #region "Width"
        /// -----------------------------------------------------------------------------
        /// <summary>
        /// The width of the form
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// This value is measured in millimeters
        /// </remarks>
        /// <history>
        /// 	[Duncan]	19/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public int Width
        {
            get { return _fi1.Width; }
            set
            {
                if (value != _fi1.Width)
                {
                    _fi1.Width = value;
                    SaveForm();
                }
            }
        }
        #endregion

        #region "Height"
        /// -----------------------------------------------------------------------------
        /// <summary>
        /// The height of the printer form
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// This value is measured in millimeters
        /// </remarks>
        /// <history>
        /// 	[Duncan]	19/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public int Height
        {
            get { return _fi1.Height; }
            set
            {
                if (value != _fi1.Height)
                {
                    _fi1.Height = value;
                    SaveForm();
                }
            }
        }
        #endregion

        #region "ImageableArea"
        /// -----------------------------------------------------------------------------
        /// <summary>
        /// Specifies the width and height, in thousandths of millimeters, of the form. 
        /// </summary>
        /// <value></value>
        /// <remarks>
        /// This may be smaller than the height and width if there is a non-printable margin
        /// on the form
        /// </remarks>
        /// <history>
        /// 	[Duncan]	19/11/2005	Created
        /// </history>
        /// -----------------------------------------------------------------------------
        public Rectangle ImageableArea
        {
            get { return new Rectangle(Convert.ToInt32(_fi1.Left), Convert.ToInt32(_fi1.Top), Convert.ToInt32(_fi1.Right - _fi1.Left), Convert.ToInt32(_fi1.Bottom - _fi1.Top)); }
            set
            {
                var _with1 = value;
                _fi1.Left = _with1.Left;
                _fi1.Top = _with1.Top;
                _fi1.Right = _with1.Width + _with1.Left;
                _fi1.Bottom = _with1.Top + _with1.Height;
                SaveForm();
            }
        }
        #endregion

        #region "Flags related"
        public bool IsBuiltInForm
        {
            get { return (_fi1.Flags == (int)SpoolerApiConstantEnumerations.FormTypeFlags.FORM_BUILTIN); }
        }

        public bool IsUserForm
        {
            get { return (_fi1.Flags == (int)SpoolerApiConstantEnumerations.FormTypeFlags.FORM_USER); }
        }

        public bool IsPrinterForm
        {
            get { return (_fi1.Flags == (int)SpoolerApiConstantEnumerations.FormTypeFlags.FORM_PRINTER); }
        }
        #endregion

        #endregion

        #region "Public constructor"

        internal PrinterForm(IntPtr hPrinter, Int32 Flags, string Name, Int32 Width, Int32 Height, Int32 Left, Int32 Top, Int32 Right, Int32 Bottom)
        {
            _hPrinter = hPrinter;

            var _with2 = _fi1;
            _with2.Name = Name;
            _with2.Flags = Flags;
            _with2.Bottom = Bottom;
            _with2.Top = Top;
            _with2.Left = Left;
            _with2.Right = Right;
            _with2.Height = Height;
            _with2.Width = Width;

        }


        public PrinterForm(string Name)
        {
        }
        #endregion

        #region "Private methods"
        private void SaveForm()
        {
            if (!UnsafeNativeMethods.SetForm(_hPrinter, _fi1.Name, 1, _fi1))
            {
                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                {
                    Trace.WriteLine("SetForm call failed", this.GetType().ToString());
                }
                throw new Win32Exception();
            }
        }
        #endregion

    }

    /// -----------------------------------------------------------------------------
    /// Project	 : PrinterQueueWatch
    /// Class	 : PrinterFormCollection
    /// 
    /// -----------------------------------------------------------------------------
    /// <summary>
    /// A collection of PrintForm objects supported by a printer
    /// </summary>
    /// <remarks>
    /// </remarks>
    /// <example>List the print forms on the named printer
    /// <code>
    ///        Dim pi As New PrinterInformation("Microsoft Office Document Image Writer", SpoolerApiConstantEnumerations.PrinterAccessRights.PRINTER_ALL_ACCESS, True)
    ///
    ///        For pf As Integer = 0 To In pi.PrinterForms.Count - 1
    ///            Me.ListBox1.Items.Add( pi.PrinterForms(pf).Name )
    ///        Next
    /// </code>
    /// </example>
    /// <seealso cref="PrinterQueueWatch.PrinterInformation.PrinterForms" />
    /// <history>
    /// 	[Duncan]	19/11/2005	Created
    ///     [Duncan]    01/05/2014  Use IntPtr for 32/64 bit compatibility
    /// </history>
    /// -----------------------------------------------------------------------------
    [System.Runtime.InteropServices.ComVisible(false)]
    [System.Security.SuppressUnmanagedCodeSecurity()]
    public class PrinterFormCollection : System.Collections.Generic.List<PrinterForm>
    {

        #region "Public interface"
        public new PrinterForm this[int index]
        {
            get { return (PrinterForm)base[index]; }
            set { base[index] = value; }
        }

        public new void Remove(PrinterForm obj)
        {
            base.Remove(obj);
        }
        #endregion

        #region "Public constructors"


        internal PrinterFormCollection(IntPtr hPrinter)
        {
            IntPtr pForm = default(IntPtr);
            Int32 pcbNeeded = default(Int32);
            Int32 pcFormsReturned = default(Int32);
            Int32 pcbProvided = default(Int32);

            if (!UnsafeNativeMethods.EnumForms(hPrinter, 1, pForm, 0, out pcbNeeded, out pcFormsReturned))
            {
                if (pcbNeeded > 0)
                {
                    pForm = Marshal.AllocHGlobal(pcbNeeded);
                    pcbProvided = pcbNeeded;
                    if (!UnsafeNativeMethods.EnumForms(hPrinter, 1, pForm, pcbProvided, out pcbNeeded, out pcFormsReturned))
                    {
                        throw new Win32Exception();
                    }
                }
            }

            if (pcFormsReturned > 0)
            {
                //\\ Get all the monitors for the given server
                IntPtr ptNext = pForm;
                while (pcFormsReturned > 0)
                {
                    FORM_INFO_1 fi1 = new FORM_INFO_1();
                    Marshal.PtrToStructure(ptNext, fi1);
                    this.Add(new PrinterForm(hPrinter, fi1.Flags, fi1.Name, fi1.Width, fi1.Height, fi1.Left, fi1.Top, fi1.Right, fi1.Bottom));
                    ptNext = ptNext + Marshal.SizeOf(fi1);
                    pcFormsReturned -= 1;
                }
            }

            //\\ Free the allocated buffer memory
            if (pForm.ToInt64() > 0)
            {
                Marshal.FreeHGlobal(pForm);
            }

        }
        #endregion

    }
}
