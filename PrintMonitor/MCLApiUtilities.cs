﻿using System;
using System.Security.Principal;

namespace PrintMonitor
{
    static class MCLApiUtilities
    {

        public static bool LoggedInAsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            try
            {
                return principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (Exception e)
            {
                return false;
            }

        }


    }
}