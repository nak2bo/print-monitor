﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.ComponentModel;
using SpoolerStructs;
using SpoolerApiConstantEnumerations;
using System.Collections;

namespace PrintMonitor
{
    [StructLayout(LayoutKind.Sequential)]
    class PrinterNotifyInfoData
    {
        public Int16 wType;
        public Int16 wField;
        public Int32 dwReserved;
        public Int32 dwId;
        public Int32 cbBuff;

        public IntPtr pBuff;

        public PrinterNotifyInfoData(IntPtr lpAddress)
        {
            if (PrinterNotifyInfoData.TraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("New(" + lpAddress.ToString() + ")", this.GetType().ToString());
            }
            Marshal.PtrToStructure(lpAddress, this);

        }

        public Printer_Notification_Types Type
        {
            get { return (Printer_Notification_Types)wType; }
        }

        public Job_Notify_Field_Indexes Field
        {
            get { return (Job_Notify_Field_Indexes)wField; }
        }

        public override string ToString()
        {
            return Marshal.PtrToStringAnsi(pBuff);
        }

        public Int32 ToInt32()
        {
            return cbBuff;
        }

        #region "Tracing"
        #endregion
        public static TraceSwitch TraceSwitch = new TraceSwitch("PrinterNotifyInfoData", "Printer Monitor Component Tracing");

    }

    [StructLayout(LayoutKind.Sequential)]
    class PrinterNotifyInfo
    {

        #region "Private member variables"

        private ArrayList colPrintJobs;
        private PRINTER_NOTIFY_INFO msInfo = new PRINTER_NOTIFY_INFO();

        private int mlPrinterInfoChanged = 0;
        #endregion


        public PrinterNotifyInfo(IntPtr mhPrinter, IntPtr lpAddress, PrintJobCollection PrintJobs) //ref PrintJobCollection PrintJobs
        {
            if (PrinterNotifyInfoData.TraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("New(" + mhPrinter.ToString() + "," + lpAddress.ToString() + ")", this.GetType().ToString());
            }

            if (!(lpAddress.ToInt64() == 0))
            {
                //\\ Create the array list of jobs involved in this event
                colPrintJobs = new ArrayList();

                //\\ Read the data of this printer notification event
                Marshal.PtrToStructure(lpAddress, msInfo);

                int nInfoDataItem = 0;
                //\\ Offset the pointer by the size of this class
                IntPtr lOffset = lpAddress + Marshal.SizeOf(msInfo);

                //\\ Process the .adata array
                for (nInfoDataItem = 0; nInfoDataItem <= msInfo.Count - 1; nInfoDataItem++)
                {
                    PrinterNotifyInfoData itemdata = new PrinterNotifyInfoData(lOffset);

                    if (itemdata.Type == Printer_Notification_Types.JOB_NOTIFY_TYPE)
                    {
                        if (itemdata.dwId == 0)
                        {
                            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceWarning)
                            {
                                Trace.WriteLine("JOB_NOTIFY_TYPE has zero job id ");
                            }
                        }

                        PrintJob pjThis = default(PrintJob);
                        //\\ If this job is not on the printer job list, add it...
                        pjThis = PrintJobs.AddOrGetById(itemdata.dwId, mhPrinter);

                        if (!colPrintJobs.Contains(itemdata.dwId))
                        {
                            colPrintJobs.Add(itemdata.dwId);
                        }


                        if ((pjThis != null))
                        {
                            var _with1 = pjThis;
                            switch (itemdata.Field)
                            {
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_PRINTER_NAME:
                                    _with1.InitPrinterName = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_USER_NAME:
                                    _with1.InitUsername = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_MACHINE_NAME:
                                    _with1.InitMachineName = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_DATATYPE:
                                    _with1.InitDataType = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_DOCUMENT:
                                    _with1.InitDocument = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_DRIVER_NAME:
                                    _with1.InitDrivername = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_NOTIFY_NAME:
                                    _with1.InitNotifyUsername = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_PAGES_PRINTED:
                                    _with1.InitPagesPrinted = itemdata.ToInt32();
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_PARAMETERS:
                                    _with1.InitParameters = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_POSITION:
                                    _with1.InitPosition = itemdata.ToInt32();
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_PRINT_PROCESSOR:
                                    _with1.InitPrintProcessorName = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_PRIORITY:
                                    _with1.InitPriority = itemdata.ToInt32();
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_STATUS:
                                    _with1.InitStatus = itemdata.ToInt32();
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_STATUS_STRING:
                                    _with1.InitStatusDescription = Marshal.PtrToStringUni(itemdata.pBuff);
                                    break;
                                case Job_Notify_Field_Indexes.JOB_NOTIFY_FIELD_TOTAL_PAGES:
                                    _with1.InitTotalPages = itemdata.ToInt32();
                                    break;
                                default:
                                    break;
                                    //\\ These are not available except where the print job is "live" 
                            }
                        }
                    }
                    else
                    {
                        //\\ Printer Info changed event
                        mlPrinterInfoChanged = Convert.ToInt32(mlPrinterInfoChanged + (Math.Pow(2, (double)itemdata.Field)));
                    }
                    lOffset = lOffset + Marshal.SizeOf(itemdata);
                }

                //\\ And free the associated memory
                if (!UnsafeNativeMethods.FreePrinterNotifyInfo(lpAddress))
                {
                    if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                    {
                        Trace.WriteLine("FreePrinterNotifyInfo(" + lpAddress.ToString() + ") failed", this.GetType().ToString());
                    }
                    throw new Win32Exception();
                }
            }

        }

        public Int32 Flags
        {
            get { return (msInfo.Flags); }
        }

        public ArrayList PrintJobs
        {
            get { return colPrintJobs; }
        }

        public bool PrinterInfoChanged
        {
            get { return (mlPrinterInfoChanged > 0); }
        }

        internal int PrinterInfoChangeFlags
        {
            get { return mlPrinterInfoChanged; }
        }

    }
}
