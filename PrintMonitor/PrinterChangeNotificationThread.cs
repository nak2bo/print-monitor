﻿using System;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading;
using SpoolerApiConstantEnumerations;

namespace PrintMonitor
{
    internal class PrinterChangeNotificationThread : IDisposable
    {

        #region "Private constants"

        private const uint INFINITE_THREAD_TIMEOUT = 0xffffffff;

        private const int PRINTER_NOTIFY_OPTIONS_REFRESH = 0x1;
        #endregion

        #region "Private members"

        private Thread _Thread;

        private IntPtr _PrinterHandle;

        private uint _ThreadTimeout = INFINITE_THREAD_TIMEOUT;
        private bool _Cancelled = false;

        private AutoResetEvent _WaitHandle;
        private PrinterMonitorComponent.MonitorJobEventInformationLevels _MonitorLevel;

        private Int32 _WatchFlags;
        private PrinterNotifyOptions _PrinterNotifyOptions;

        private PrinterInformation _PrinterInformation;

        private bool _PauseAllNewPrintJobs;
        #endregion
        private static object _NotificationLock = new object();

        #region "Public methods"

        //\\ Start a watching thread
        //ByVal PrinterHandle As Int32)
        public void StartWatching()
        {

            _Thread = new Thread(this.StartThread);
            try
            {
                _Thread.SetApartmentState(ApartmentState.STA);
            }
            catch (Exception e)
            {
                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                {
                    Trace.WriteLine("Failed to set Single Threaded Apartment for : " + _Thread.Name, this.GetType().ToString());
                }
            }
            _Thread.Name =  this.GetType().Name + ":" + _PrinterHandle.ToString();
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("StartWatching created new thread: " + _Thread.Name, this.GetType().ToString());
            }
            _Thread.Start();

        }

        //\\ Cancel the watching thread
        public void CancelWatching()
        {
            _Cancelled = true;
            if ((_WaitHandle != null))
            {
                if (!(_WaitHandle.SafeWaitHandle.IsInvalid || _WaitHandle.SafeWaitHandle.IsClosed))
                {
                    _WaitHandle.Set();
                }
            }
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("CancelWatching()", this.GetType().ToString());
            }

        }

        public void StopWatching()
        {
            CancelWatching();


            //\\ And free the printer change notification handle (if it exists)
            if ((_WaitHandle != null))
            {
                if (!(_WaitHandle.SafeWaitHandle.IsInvalid) || (_WaitHandle.SafeWaitHandle.IsClosed))
                {
                    //\\ Stop monitoring the printer
                    try
                    {
                        if (UnsafeNativeMethods.FindClosePrinterChangeNotification(_WaitHandle.SafeWaitHandle))
                        {
                            _WaitHandle.Close();
                            _WaitHandle = null;
                        }
                        else
                        {
                            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                            {
                                Trace.WriteLine("FindClosePrinterChangeNotification() failed", this.GetType().ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                        {
                            Trace.WriteLine(ex.ToString(), this.GetType().ToString());
                        }
                    }
                }
            }

            //\\ Wait for the monitoring thread to terminate
            if ((_Thread != null))
            {
                if (_Thread.IsAlive)
                {
                    _Thread.Join();
                }
            }

            GC.KeepAlive(_WaitHandle);
            //\\ And explicitly unset it
            _Thread = null;

        }

        public override string ToString()
        {
            if ((_WaitHandle != null))
            {
                return this.GetType().ToString() + " handle is valid";
            }
            else
            {
                return this.GetType().ToString() + " handle invalid ";
            }
        }
        #endregion

        #region "Public interface"
        #region "PauseAllNewPrintJobs"
        public bool PauseAllNewPrintJobs
        {
            get { return _PauseAllNewPrintJobs; }
            set { _PauseAllNewPrintJobs = value; }
        }
        #endregion
        #endregion

        #region "Public constructors"


        public PrinterChangeNotificationThread(IntPtr PrinterHandle, uint ThreadTimeout, PrinterMonitorComponent.MonitorJobEventInformationLevels MonitorLevel, int WatchFlags, PrinterInformation PrinterInformation) //ref PrinterInformation PrinterInformation
        {
            //\\ Save a local copy of the information passed in...
            _PrinterHandle = PrinterHandle;
            _ThreadTimeout = ThreadTimeout;
            _MonitorLevel = MonitorLevel;
            _WatchFlags = WatchFlags;
            _PrinterInformation = PrinterInformation;

        }


        public PrinterChangeNotificationThread()
        {
        }

        #endregion

        #region "Private methods"
        //\\ StartThread - The entry point for this thread

        private void StartThread()
        {
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("StartThread() of printer handle :" + _PrinterHandle.ToString(), this.GetType().ToString());
            }

            if (_PrinterHandle.ToInt64() == 0)
            {
                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                {
                    Trace.WriteLine("StartThread(): _PrinterHandle not set", this.GetType().ToString());
                }
                _Cancelled = true;
                return;
            }

            //\\ Initialise the printer change notification
            Microsoft.Win32.SafeHandles.SafeWaitHandle mhWait = null;

            if (_WatchFlags == 0)
            {
                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceWarning)
                {
                    Trace.WriteLine("StartWatch: No watch flags set - defaulting to PRINTER_CHANGE_JOB or PRINTER_CHANGE_PRINTER ", this.GetType().ToString());
                }
                _WatchFlags = Convert.ToInt32(PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_JOB | PrinterChangeNotificationGeneralFlags.PRINTER_CHANGE_PRINTER);
            }

            //\\ Specify what we want to be notified about...
            if (_MonitorLevel == PrinterMonitorComponent.MonitorJobEventInformationLevels.MaximumJobInformation)
            {
                _PrinterNotifyOptions = new PrinterNotifyOptions(false);
            }
            else
            {
                _PrinterNotifyOptions = new PrinterNotifyOptions(true);
            }

            if (_MonitorLevel == PrinterMonitorComponent.MonitorJobEventInformationLevels.MaximumJobInformation | _MonitorLevel == PrinterMonitorComponent.MonitorJobEventInformationLevels.MinimumJobInformation)
            {
                try
                {
                    mhWait = UnsafeNativeMethods.FindFirstPrinterChangeNotification(_PrinterHandle, _WatchFlags, 0, _PrinterNotifyOptions);

                }
                catch (Win32Exception e)
                {
                    // An operating system error has been trapped
                    if (PrinterMonitorComponent.ComponentTraceSwitch.Level >= TraceLevel.Error)
                    {
                        Trace.WriteLine(e.Message + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                    }
                }
                catch (Exception e2)
                {
                    if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                    {
                        Trace.WriteLine(e2.Message + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                    }
                }
                finally
                {
                    if ((mhWait != null) && mhWait.IsInvalid)
                    {
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                        {
                            Trace.WriteLine("StartWatch: FindFirstPrinterChangeNotification failed - handle: " + mhWait.ToString() + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                        }
                        throw new Win32Exception();
                    }
                    else
                    {
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                        {
                            Trace.WriteLine("StartWatch: FindFirstPrinterChangeNotification succeeded - handle: " + mhWait.ToString() + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                        }
                    }
                }

            }
            else if (_MonitorLevel == PrinterMonitorComponent.MonitorJobEventInformationLevels.NoJobInformation)
            {
                try
                {
                    mhWait = UnsafeNativeMethods.FindFirstPrinterChangeNotification(_PrinterHandle, _WatchFlags, 0, IntPtr.Zero);

                }
                catch (Win32Exception e)
                {
                    //\\ An operating system error has been trapped and returned
                    if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                    {
                        Trace.WriteLine(e.Message + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                    }
                }
                catch (Exception e2)
                {
                    if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                    {
                        Trace.WriteLine(e2.Message + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                    }
                }
                finally
                {
                    if (mhWait.IsInvalid)
                    {
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                        {
                            Trace.WriteLine("StartWatch: FindFirstPrinterChangeNotification failed - handle: " + mhWait.ToString() + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                        }
                        throw new Win32Exception();
                    }
                    else
                    {
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceInfo)
                        {
                            Trace.WriteLine("StartWatch: FindFirstPrinterChangeNotification succeeded - handle: " + mhWait.ToString() + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                        }
                    }
                }
            }

            if (mhWait.IsInvalid || mhWait.IsClosed)
            {
                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                {
                    Trace.WriteLine("StartWatch: FindFirstPrinterChangeNotification failed for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                }
                _Cancelled = true;
                return;
            }
            else
            {
                if (_WaitHandle == null)
                {
                    _WaitHandle = new AutoResetEvent(false);
                }
                _WaitHandle.SafeWaitHandle = mhWait;
            }

            while (!_Cancelled)
            {
                try
                {
                    //\\ Wait for the WaitHandle to trigger
                    if (_WaitHandle.WaitOne(-1, false))
                    {
                        //\\ if the wait handle has not timed out
                        if (!_Cancelled)
                        {
                            DecodePrinterChangeInformation();
                        }
                        else
                        {
                            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                            {
                                Trace.WriteLine("StartThread: Cancelled monitoring raised an event " + _PrinterHandle.ToString(), this.GetType().ToString());
                            }
                        }
                    }
                    else
                    {
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                        {
                            Trace.WriteLine("Wait handle timed out", this.GetType().ToString());
                        }
                    }
                }
                catch (ThreadAbortException eTAE)
                {
                    //\\ This occurs if the thread was aborted from an external source
                    if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                    {
                        Trace.WriteLine("PrinterNotificationThread aborted", this.GetType().ToString());
                    }
                    //\\ Therefore stop watching
                    _Cancelled = true;
                }
            }

            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("PrinterNotificationThread loop ended", this.GetType().ToString());
            }

        }

        /// <summary>
        /// When the spooler notifies the monitoring thread that there is a printer change event 
        /// signalled, this sub decodes that change event and posts it on the event queue
        /// </summary>
        /// <remarks></remarks>
        private void DecodePrinterChangeInformation()
        {
            Int32 mpdChangeFlags = default(Int32);
            IntPtr mlpPrinter = default(IntPtr);
            PrinterNotifyInfo pInfo = default(PrinterNotifyInfo);
            PrinterEventFlagDecoder piEventFlags = default(PrinterEventFlagDecoder);

            if (_WaitHandle == null || _WaitHandle.SafeWaitHandle.IsClosed || _WaitHandle.SafeWaitHandle.IsInvalid)
            {
                return;
            }

            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("DecodePrinterChangeInformation() for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
            }


            //\\ Prevent this code being re-entrant...
            lock (_NotificationLock)
            {


                //\\ A printer change notification has occured.....
                try
                {

                    if (UnsafeNativeMethods.FindNextPrinterChangeNotification(_WaitHandle.SafeWaitHandle, out mpdChangeFlags, _PrinterNotifyOptions, out mlpPrinter))
                    {

                        if (mlpPrinter.ToInt64() != 0)
                        {
                            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                            {
                                Trace.WriteLine("FindNextPrinterChangeNotification returned a pointer to PRINTER_NOTIFY_INFO :" + mlpPrinter.ToString() + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                            }

                            pInfo = new PrinterNotifyInfo(_PrinterHandle, mlpPrinter, _PrinterInformation.PrintJobs);

                            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                            {
                                Trace.WriteLine("FindNextPrinterChangeNotification notify info " + pInfo.Flags.ToString());
                            }

                            piEventFlags = new PrinterEventFlagDecoder(pInfo.Flags);

                            //\\ If the flags indicate that there was insufficient space to store all
                            //\\ the changes we need to ask again
                            if (!piEventFlags.IsInfoComplete)
                            {
                                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                                {
                                    Trace.WriteLine("FindNextPrinterChangeNotification returned incomplete PRINTER_NOTIFY_INFO" + " for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                                }

                                _PrinterNotifyOptions.dwFlags = _PrinterNotifyOptions.dwFlags | PRINTER_NOTIFY_OPTIONS_REFRESH;

                                if (!UnsafeNativeMethods.FindNextPrinterChangeNotification(_WaitHandle.SafeWaitHandle, out mpdChangeFlags, _PrinterNotifyOptions, out mlpPrinter))
                                {
                                    if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                                    {
                                        Trace.WriteLine("FindNextPrinterChangeNotification failed to PRINTER_NOTIFY_OPTIONS_REFRESH for printer handle: " + _PrinterHandle.ToString(), this.GetType().ToString());
                                    }
                                    throw new Win32Exception();
                                }
                                else
                                {
                                    _PrinterNotifyOptions.dwFlags = _PrinterNotifyOptions.dwFlags & (~PRINTER_NOTIFY_OPTIONS_REFRESH);
                                    if (mlpPrinter.ToInt64() != 0)
                                    {
                                        pInfo = new PrinterNotifyInfo(_PrinterHandle, mlpPrinter, _PrinterInformation.PrintJobs);
                                    }
                                }
                            }


                            //\\ Raise the appropriate event...
                            piEventFlags = new PrinterEventFlagDecoder(mpdChangeFlags);
                            int nIndex = 0;
                            PrintJob thisJob = default(PrintJob);


                            if (piEventFlags.ChangesOccured)
                            {
                                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                                {
                                    Trace.WriteLine("piEventFlags set - changes have occured ");
                                }

                                for (nIndex = 0; nIndex <= pInfo.PrintJobs.Count - 1; nIndex++)
                                {
                                    thisJob = _PrinterInformation.PrintJobs.ItemByJobId(Convert.ToInt32(pInfo.PrintJobs[nIndex]));
                                    if ((thisJob != null))
                                    {
                                        if (thisJob.JobId != 0)
                                        {
                                            if (piEventFlags.JobAdded)
                                            {
                                                if (_PauseAllNewPrintJobs)
                                                {
                                                    thisJob.Paused = true;
                                                }
                                                PrintJobEventArgs e = new PrintJobEventArgs(thisJob, PrintJobEventArgs.PrintJobEventTypes.JobAddedEvent);
                                                _PrinterInformation.EventQueue.AddJobEvent(e);
                                            }
                                            if (piEventFlags.JobSet)
                                            {
                                                PrintJobEventArgs e = new PrintJobEventArgs(thisJob, PrintJobEventArgs.PrintJobEventTypes.JobSetEvent);
                                                _PrinterInformation.EventQueue.AddJobEvent(e);
                                            }
                                            if (piEventFlags.JobWritten)
                                            {
                                                PrintJobEventArgs e = new PrintJobEventArgs(thisJob, PrintJobEventArgs.PrintJobEventTypes.JobWrittenEvent);
                                                _PrinterInformation.EventQueue.AddJobEvent(e);
                                            }
                                            if (piEventFlags.JobDeleted)
                                            {
                                                PrintJobEventArgs e = new PrintJobEventArgs(thisJob, PrintJobEventArgs.PrintJobEventTypes.JobDeletedEvent);
                                                _PrinterInformation.EventQueue.AddJobEvent(e);
                                                var _with1 = _PrinterInformation.PrintJobs;
                                                //\\ Remove this item from the printjobs collection
                                                _with1.JobPendingDeletion = Convert.ToInt32(pInfo.PrintJobs[nIndex]);
                                            }
                                        }
                                    }
                                }

                                if (piEventFlags.PrinterChange > 0)
                                {
                                    //\\ If the printer info changed throw that event
                                    PrinterEventArgs pe = new PrinterEventArgs(pInfo.PrinterInfoChangeFlags, _PrinterInformation);
                                    _PrinterInformation.EventQueue.AddPrinterEvent(pe);
                                }

                                // -- SERVER_EVENTS ----------------------------------------------
                                // Not implemented in this release
#if SERVER_EVENTS //= 1
							if (piEventFlags.DriverChange > 0) {
								//TODO: Pass on the appropriate driver event
								PrintServerDriverEventArgs pde = new PrintServerDriverEventArgs(piEventFlags.DriverChange);

							}

							if (piEventFlags.FormChange > 0) {
								//TODO: Pass on the appropriate form change event
								PrintServerFormEventArgs pfe = new PrintServerFormEventArgs(piEventFlags.FormChange);

							}

							if (piEventFlags.PortChange > 0) {
								//TODO: Pass on the appropriate port change event
								PrintServerPortEventArgs ppe = new PrintServerPortEventArgs(piEventFlags.PortChange);

							}

							if (piEventFlags.ProcessorChange > 0) {
								//TODO: Pass on the appropriate processor change event
								PrintServerProcessorEventArgs ppce = new PrintServerProcessorEventArgs(piEventFlags.ProcessorChange);
							}
#endif

                            }
                            piEventFlags = null;
                        }
                        else
                        {
                            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                            {
                                Trace.WriteLine("FindNextPrinterChangeNotification did not return a pointer to PRINTER_NOTIFY_INFO - the change flag was:" + _WatchFlags.ToString(), this.GetType().ToString());
                            }
                            piEventFlags = new PrinterEventFlagDecoder(mpdChangeFlags);
                            if (piEventFlags.PrinterChange > 0)
                            {
                                //\\ If the printer info changed throw that event
                                PrinterEventArgs pe = new PrinterEventArgs(0, _PrinterInformation);
                                _PrinterInformation.EventQueue.AddPrinterEvent(pe);
                            }
                        }

                        _PrinterInformation.EventQueue.Awaken();
                    }
                    else
                    {
                        //\\ Failed to get the next printer change notification set up...
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                        {
                            Trace.WriteLine("FindNextPrinterChangeNotification failed", this.GetType().ToString());
                        }
                        CancelWatching();
                    }
                }
                catch (Exception exP)
                {
                    if (PrinterMonitorComponent.ComponentTraceSwitch.TraceError)
                    {
                        Trace.WriteLine("FindNextPrinterChangeNotification failed : " + exP.ToString(), this.GetType().ToString());
                    }
                }
            }
        }
        #endregion

        #region "IDisposable interface"
        public void Dispose()
        {
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("Dispose()", this.GetType().ToString());
            }
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                StopWatching();
                if ((_PrinterNotifyOptions != null))
                {
                    _PrinterNotifyOptions.Dispose();
                    _PrinterNotifyOptions = null;
                }

            }
        }

        //protected override void Finalize()
        //{
        //    Dispose(false);
        //}

        #endregion

    }
}
