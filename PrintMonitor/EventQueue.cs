﻿using System;
using System.Diagnostics;
using System.Threading;

namespace PrintMonitor
{
    internal class EventQueue : IDisposable
    {
        #region "PrinterEventQueue class"
        private class PrinterEventQueue : System.Collections.Concurrent.ConcurrentQueue<EventArgs>
        {

            public bool Contains(PrintJobEventArgs JobEventArgs)
            {

                //\\ Duplicate JobWritten events can be ignored
                if (JobEventArgs.EventType == PrintJobEventArgs.PrintJobEventTypes.JobWrittenEvent)
                {
                    return false;
                }

                //\\ Because an EventQueue pertains to only one printer we can compare on job id and event type
                object oItem = null;
                try
                {
                    foreach (object oItem_loopVariable in this)
                    {
                        oItem = oItem_loopVariable;
                        if (oItem is PrintJobEventArgs)
                        {
                            var _with1 = (PrintJobEventArgs)oItem;
                            if (JobEventArgs.EventType == _with1.EventType && JobEventArgs.PrintJob.JobId == _with1.PrintJob.JobId && JobEventArgs.PrintJob.QueuedTime == _with1.PrintJob.QueuedTime)
                            {
                                return true;
                            }
                        }
                    }
                }
                catch (Exception es)
                {
                    return false;
                }


                return false;
            }

        }
        #endregion

        #region "Private member variables"

        private PrinterEventQueue _EventQueue = new PrinterEventQueue();

        private Thread _EventQueueWorker;
        private PrinterMonitorComponent.JobEvent _JobEvent;

        private PrinterMonitorComponent.PrinterEvent _PrinterEvent;
        private AutoResetEvent _WaitHandle;

        private static bool _Cancelled;

        #endregion

        #region "Public interface"
        #region "AddJobEvent"
        public void AddJobEvent(PrintJobEventArgs JobEventArgs)
        {
            //\\ Job events must be unique
            if (!_EventQueue.Contains(JobEventArgs))
            {
                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                {
                    Trace.WriteLine("Job event enqueued : " + JobEventArgs.EventType.ToString(), this.GetType().ToString());
                }
                //\\ If the job didn't populate properly, try again
                var _with2 = JobEventArgs.PrintJob;
                if (!_with2.Populated)
                {
                    _with2.Refresh();
                }
                _EventQueue.Enqueue(JobEventArgs);
            }
        }
        #endregion

        #region "AddPrinterEvent"
        public void AddPrinterEvent(PrinterEventArgs PrinterEventArgs)
        {
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("Printer event enqueued", this.GetType().ToString());
            }
            _EventQueue.Enqueue(PrinterEventArgs);
        }
        #endregion

        #region "Awaken"
        //\\ --[Awaken]--------------------------------------------------------
        //\\ Wakes the thread which dequeues and processes the events
        //\\ ------------------------------------------------------------------
        public void Awaken()
        {
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("Event queue awakened - " + _EventQueue.Count.ToString() + " events ", this.GetType().ToString());
            }
            if ((_WaitHandle != null) && EventsPending)
            {
                _WaitHandle.Set();
            }
        }
        #endregion

        #region "EventsPending"
        /// <summary>
        /// Returns True if there are any printer or print job events queued that should be processed 
        /// </summary>
        /// <value></value>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool EventsPending
        {
            get
            {
                bool eventsPendingProcessing = (_EventQueue.Count > 0);
                return eventsPendingProcessing;
            }
        }
        #endregion

        #region "OnEndInvokeJobEvent"
        public void OnEndInvokeJobEvent(IAsyncResult ar)
        {
            _JobEvent.EndInvoke(ar);
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("Job event returned", this.GetType().ToString());
            }
        }
        #endregion

        #region "OnEndInvokePriterEvent"
        public void OnEndInvokePrinterEvetnt(IAsyncResult ar)
        {
            _PrinterEvent.EndInvoke(ar);
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("Printer event returned", this.GetType().ToString());
            }
        }
        #endregion

        #endregion

        #region "Private methods"

        private void StartThread()
        {
            if (_WaitHandle == null)
            {
                _WaitHandle = new AutoResetEvent(false);
            }

            while (!_Cancelled)
            {
                try
                {
                    //\\ Wait for the WaitHandle to trigger
                    if (_WaitHandle.WaitOne(500, false))
                    {
                        //\\ if the wait handle has not timed out
                        ProcessQueue();
                    }
                }
                catch (ThreadAbortException eTA)
                {
                    //\\ The thread was aborted prematurely
                    if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                    {
                        Trace.WriteLine("EventQueue loop aborted prematurely", this.GetType().ToString());
                    }
                    _Cancelled = true;
                }
            }
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("EventQueue loop ended", this.GetType().ToString());
            }

        }

        private void ProcessQueue()
        {
            EventArgs oEvent = null;
            IAsyncResult ar = default(IAsyncResult);

            _WaitHandle.Reset();


            while (_EventQueue.Count > 0)
            {
                if (_EventQueue.TryDequeue(out oEvent))
                {
                    if ((oEvent) is PrintJobEventArgs)
                    {
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                        {
                            Trace.WriteLine("Job event dequeued : " + ((PrintJobEventArgs)oEvent).EventType.ToString(), this.GetType().ToString());
                        }
                        ar = _JobEvent.BeginInvoke((PrintJobEventArgs)oEvent, OnEndInvokeJobEvent, null);
                    }
                    else if ((oEvent) is PrinterEventArgs)
                    {
                        if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                        {
                            Trace.WriteLine("Printer event dequeued", this.GetType().ToString());
                        }
                        ar = _PrinterEvent.BeginInvoke((PrinterEventArgs)oEvent, OnEndInvokePrinterEvetnt, null);
                    }
                }
            }

            //\\ If events have arrived since we started dequeueing them then triger the wait handle again to deal with them
            if (EventsPending)
            {
                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                {
                    Trace.WriteLine("Events pending while dequeueing ", this.GetType().ToString());
                }
                _WaitHandle.Set();
            }
        }
        #endregion

        #region "Public constructor"

        public EventQueue(PrinterMonitorComponent.JobEvent JobEvent, PrinterMonitorComponent.PrinterEvent PrinterEvent)
        {
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("New()", this.GetType().ToString());
            }

            _JobEvent = JobEvent;
            _PrinterEvent = PrinterEvent;

            _EventQueueWorker = new Thread(this.StartThread);
            try
            {
                _EventQueueWorker.SetApartmentState(ApartmentState.STA);
            }
            catch (Exception e)
            {
                if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
                {
                    Trace.WriteLine("Failed to set Single Threaded Apartment for : " + _EventQueueWorker.Name, this.GetType().ToString());
                }
            }
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("StartWatching created new EventQueueWorker", this.GetType().ToString());
            }
            _EventQueueWorker.Start();
        }
        #endregion

        #region "IDisposable interface"
        public void Dispose()
        {
            if (PrinterMonitorComponent.ComponentTraceSwitch.TraceVerbose)
            {
                Trace.WriteLine("Dispose()", this.GetType().ToString());
            }
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Shutdown();
                if (((_WaitHandle != null)))
                {
                    _WaitHandle.Dispose();
                    _WaitHandle = null;
                }
            }


        }

        //protected override void Finalize()
        //{
        //    Dispose(false);
        //}

        #region "Shutdown"

        public void Shutdown()
        {
            _Cancelled = true;
            if ((_WaitHandle != null))
            {
                _WaitHandle.Set();
            }
            if ((_EventQueueWorker != null))
            {
                _EventQueueWorker.Join();
                _EventQueueWorker = null;
            }
            _WaitHandle = null;
            _EventQueueWorker = null;
            _JobEvent = null;
            _PrinterEvent = null;
        }

        #endregion
        #endregion

    }
}
